﻿class SoulReaver:

    speed = 3
    position = None
    size = None

    def __init__(self, position, size):
        self.position = position
        self.size = size
    
    def move(self):

        v = GetNextPosition() - self.position
        self.position += v.Normalized() * self.speed

        FaceDirection(-v.X)

