<?xml version="1.0" encoding="UTF-8"?>
<tileset name="HarvesTiles" tilewidth="32" tileheight="32" tilecount="21" columns="0">
 <tile id="0">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile000.png"/>
 </tile>
 <tile id="1">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile001.png"/>
 </tile>
 <tile id="2">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile002.png"/>
 </tile>
 <tile id="3">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile003.png"/>
 </tile>
 <tile id="4">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile004.png"/>
 </tile>
 <tile id="5">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile005.png"/>
 </tile>
 <tile id="6">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile006.png"/>
 </tile>
 <tile id="7">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile007.png"/>
 </tile>
 <tile id="8">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile008.png"/>
 </tile>
 <tile id="9">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile009.png"/>
 </tile>
 <tile id="10">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile010.png"/>
 </tile>
 <tile id="11">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile011.png"/>
 </tile>
 <tile id="12">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile012.png"/>
 </tile>
 <tile id="13">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile013.png"/>
 </tile>
 <tile id="14">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile014.png"/>
 </tile>
 <tile id="15">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile015.png"/>
 </tile>
 <tile id="16">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile016.png"/>
 </tile>
 <tile id="17">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile017.png"/>
 </tile>
 <tile id="18">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile018.png"/>
 </tile>
 <tile id="19">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile019.png"/>
 </tile>
 <tile id="20">
  <image width="32" height="32" source="../Raw/Graphics/tiles/platformPack_tile020.png"/>
 </tile>
</tileset>
