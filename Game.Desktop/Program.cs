﻿using System;
using AmosDesktop;

namespace Game.Desktop
{
    class Program
    {
        static void Main(String[] args)
        {
            HGame game = new HGame();
            using (GameWindow window = new GameWindow(game.InitialResolution, 1f, game, "The Harvest"))
            {
                window.Run();
            }
        }
    }
}
