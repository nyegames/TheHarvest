﻿using System;
using System.Linq;
using AmosShared.Events;
using AmosShared.Interfaces;
using AmosShared.Managers;
using Game.Desktop.HObjects.SoulVessels;
using Game.Desktop.HObjects.Spawn;
using Game.Desktop.Scenes;
using Game.Desktop.Services.Level;
using Game.Desktop.Services.Scenes;

namespace Game.Desktop.SoulReavers
{
    /// <summary>Will gather information for all BadGuys to use in order to gain information about the world and the Character</summary>
    public class SoulReaverController : AmosManager<SoulReaverController>, IUpdatable
    {
        /// <summary>All the vessels in the current level</summary>
        private Int32 _TotalVessels;
        /// <summary>The current amount of vessels that have been collected in this level</summary>
        private Int32 _CollectedVesselCount;

        private HScene _Level;

        public Boolean IsDisposed { get; set; }

        public SoulReaverController()
        {
            EventManager.Instance.StartListen(SceneService.OnActiveSceneChanged, SceneChanged);

            EventManager.Instance.StartListen(LevelService.LevelProgressUpdated, VesselCountUpdated);
        }

        internal void Initialise()
        {

        }

        private void SceneChanged(Object[] obj)
        {
            _Level = (HScene)obj[0];
            _TotalVessels = _Level.Objects.Count(s => s is SoulVessel);
            _CollectedVesselCount = 0;

            var sp = _Level.Objects.OfType<SpawnPosition>().FirstOrDefault()?.Position ?? new OpenTK.Vector2(0, 0);

            if (!_Level.Name.StartsWith("Level")) return;

            var b = new SoulReaver($"SoulReaver-Bee")
            {
                Position = sp,
                Active = false,
                Visible = false
            };

            _Level.AddObject(b);
        }

        private void VesselCountUpdated(Object[] data)
        {
            _CollectedVesselCount = (Int32)((Object[])data)[0];

            if (_CollectedVesselCount == 1) ActivateSoulReaver();
        }

        private void ActivateSoulReaver()
        {
            foreach (var soulReaver in SceneService.Instance.ActiveScene.Objects.OfType<SoulReaver>())
            {
                soulReaver.Active = true;
                soulReaver.Visible = true;
            }
        }

        public void Update()
        {

        }

        public Boolean CanUpdate() => !IsDisposed;

        public void Dispose()
        {
            EventManager.Instance.StopListen(SceneService.OnActiveSceneChanged, SceneChanged);
            EventManager.Instance.StopListen(LevelService.LevelProgressUpdated, VesselCountUpdated);
        }
    }
}
