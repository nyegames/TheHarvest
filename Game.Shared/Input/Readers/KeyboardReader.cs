﻿using System;
using System.Collections.Generic;
using OpenTK.Input;

namespace Game.Shared.Input.Readers
{
    public class KeyboardReader : DeviceReader
    {
        private readonly Dictionary<Key, String> _RegisteredButtons;
        private KeyboardState _PreviousState;

        public KeyboardReader() : base()
        {
            _PreviousState = Keyboard.GetState();

            _RegisteredButtons = new Dictionary<Key, String>()
            {
                {Key.E, Interact },
                {Key.F, Attack },

                {Key.C, Crouch },

                {Key.A, Left },
                {Key.D, Right },

                {Key.Space, Jump },
            };
        }

        public override void Update()
        {
            var currentState = Keyboard.GetState();

            foreach (var kv in _RegisteredButtons)
            {
                Key k = kv.Key;
                String v = kv.Value;

                //If the key was previously not down, but now it is. Its been pressed
                if (!_PreviousState.IsKeyDown(k) && currentState.IsKeyDown(k))
                {
                    BehaviourPressed(v);
                }

                else if (_PreviousState.IsKeyDown(k) && currentState.IsKeyDown(k))
                {
                    BehaviourHeld(v);
                }

                else if (_PreviousState.IsKeyDown(k) && !currentState.IsKeyDown(k))
                {
                    BehaviourOff(v);
                }
            }

            _PreviousState = currentState;
        }
    }
}
