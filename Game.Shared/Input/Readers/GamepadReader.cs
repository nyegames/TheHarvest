﻿using System;
using OpenTK;
using OpenTK.Input;

namespace Game.Shared.Input.Readers
{
    public class GamepadReader : DeviceReader
    {
        private readonly Vector2 _MinDetect = new Vector2(0.5f, 0.5f);

        private GamePadState _PreviousState;

        public GamepadReader() : base()
        {

        }

        public override void Update()
        {
            var currentState = GamePad.GetState(0);

            #region leftstick
            var pLeft = _PreviousState.ThumbSticks.Left.X <= -_MinDetect.X;
            var pRight = _PreviousState.ThumbSticks.Left.X >= _MinDetect.X;

            var left = currentState.ThumbSticks.Left.X <= -_MinDetect.X;
            var right = currentState.ThumbSticks.Left.X >= _MinDetect.X;

            if (!pLeft && left) BehaviourPressed(Left);
            if (pLeft && !left) BehaviourOff(Left);
            else if (pLeft && left) BehaviourHeld(Left);

            if (!pRight && right) BehaviourPressed(Right);
            else if (pRight && right) BehaviourHeld(Right);
            else if (pRight && !right) BehaviourOff(Right);
            #endregion

            if (_PreviousState.Buttons.A == ButtonState.Released && currentState.Buttons.A == ButtonState.Pressed) BehaviourPressed(Jump);
            else if (_PreviousState.Buttons.A == ButtonState.Pressed && currentState.Buttons.A == ButtonState.Pressed) BehaviourHeld(Jump);
            else if (_PreviousState.Buttons.A == ButtonState.Pressed && currentState.Buttons.A == ButtonState.Released) BehaviourOff(Jump);

            if (_PreviousState.Buttons.Y == ButtonState.Released && currentState.Buttons.Y == ButtonState.Pressed) BehaviourPressed(Crouch);
            else if (_PreviousState.Buttons.Y == ButtonState.Pressed && currentState.Buttons.Y == ButtonState.Pressed) BehaviourHeld(Crouch);
            else if (_PreviousState.Buttons.Y == ButtonState.Pressed && currentState.Buttons.Y == ButtonState.Released) BehaviourOff(Crouch);

            if (_PreviousState.Buttons.X == ButtonState.Released && currentState.Buttons.X == ButtonState.Pressed) BehaviourPressed(Interact);
            else if (_PreviousState.Buttons.X == ButtonState.Pressed && currentState.Buttons.X == ButtonState.Pressed) BehaviourHeld(Interact);
            else if (_PreviousState.Buttons.X == ButtonState.Pressed && currentState.Buttons.X == ButtonState.Released) BehaviourOff(Interact);

            if (_PreviousState.Buttons.B == ButtonState.Released && currentState.Buttons.B == ButtonState.Pressed) BehaviourPressed(Attack);
            else if (_PreviousState.Buttons.B == ButtonState.Pressed && currentState.Buttons.B == ButtonState.Pressed) BehaviourHeld(Attack);
            else if (_PreviousState.Buttons.B == ButtonState.Pressed && currentState.Buttons.B == ButtonState.Released) BehaviourOff(Attack);

            _PreviousState = currentState;
        }
    }
}
