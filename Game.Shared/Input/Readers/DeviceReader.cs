﻿using System;
using AmosShared.Interfaces;

namespace Game.Shared.Input.Readers
{
    public abstract class DeviceReader : IUpdatable
    {
        public Boolean IsDisposed { get; set; }

        protected const String Interact = "Interact";
        protected const String Attack = "Attack";

        protected const String Crouch = "Crouch";

        protected const String Left = "Left";
        protected const String Right = "Right";

        protected const String Jump = "Jump";

        public Boolean CanUpdate() => !IsDisposed;

        public abstract void Update();

        protected void BehaviourPressed(String name)
        {
            InputService.Instance.BehaviourChanged(name, InputBehaviour.State.Pressed);
        }

        protected void BehaviourHeld(String name)
        {
            InputService.Instance.BehaviourChanged(name, InputBehaviour.State.Held);
        }

        protected void BehaviourOff(String name)
        {
            InputService.Instance.BehaviourChanged(name, InputBehaviour.State.Off);
        }

        public void Dispose()
        {
            IsDisposed = true;
        }
    }
}
