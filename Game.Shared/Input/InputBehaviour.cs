﻿using System;
using AmosShared.Base;
using AmosShared.Interfaces;

namespace Game.Shared.Input
{
    public class InputBehaviour : IUpdatable
    {
        public enum State
        {
            Off,
            Pressed,
            Held
        }

        public Boolean IsDisposed { get; set; } = false;

        public State BehaviourState { get; private set; } = State.Off;

        public TimeSpan HeldTime { get; private set; } = TimeSpan.Zero;

        public void SetState(State behaviourState)
        {
            if (behaviourState == State.Off) HeldTime = TimeSpan.Zero;
            BehaviourState = behaviourState;
        }

        public void Dispose()
        {
            IsDisposed = true;
        }

        public void Update()
        {
            if (BehaviourState == State.Held)
            {
                HeldTime += GameTime.DeltaTime;
            }
        }

        public Boolean CanUpdate() => !IsDisposed;
    }
}
