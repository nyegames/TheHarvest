﻿using System;
using AmosShared.Base;
using OpenTK.Input;

namespace Game.Shared.Input
{
    /// <summary>Data object to hold information about the <see cref="Key"/> currently being held</summary>
    internal class KeyData
    {
        public KeyData(TimeSpan time, Boolean held, Key key)
        {
            HeldTime = time;
            Held = held;
            Key = key;
        }
        /// <summary>Whether this Key has just been pressed, will be reset to false once its classed as Held</summary>
        public Boolean Pressed { get; set; }
        /// <summary>Whether or not this is currently being held</summary>
        public Boolean Held { get; set; }
        /// <summary>The total time this has been held for</summary>
        public TimeSpan HeldTime { get; private set; }

        /// <summary>The current  since last frame, while this is being held</summary>
        public TimeSpan Delta { get; private set; }
        /// <summary>The <see cref="Key"/> this information is about</summary>
        public Key Key { get; private set; }

        public void IncreaseHeldTime()
        {
            Delta = GameTime.DeltaTime;
            HeldTime += GameTime.DeltaTime;
        }

        internal void Reset()
        {
            Pressed = false;
            Held = false;
            HeldTime = TimeSpan.Zero;
            Delta = TimeSpan.Zero;
        }
    }
}
