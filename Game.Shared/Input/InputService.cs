﻿using AmosShared.Base;
using AmosShared.Interfaces;
using System;
using System.Collections.Generic;
using AmosShared.Service;
using Game.Shared.Input.Readers;

namespace Game.Shared.Input
{
    public class InputService : AService<InputService>
    {
        private readonly Dictionary<String, InputBehaviour> _ActiveBehaviours = new Dictionary<String, InputBehaviour>();

        private readonly List<DeviceReader> _Readers = new List<DeviceReader>();

        public Boolean IsDisposed { get; set; } = false;

        public InputService()
        {
            _Readers.Add(new KeyboardReader());
            _Readers.Add(new GamepadReader());
        }

        public Boolean CanUpdate() => !IsDisposed;

        public void Dispose()
        {
            IsDisposed = true;

        }

        public override void Update()
        {
            base.Update();
            foreach (var r in _Readers)
            {
                r.Update();
            }

            foreach (var b in _ActiveBehaviours)
            {
                if (b.Value.BehaviourState == InputBehaviour.State.Off) continue;
                b.Value.Update();
            }
        }

        public void BehaviourChanged(String name, InputBehaviour.State behaviourState)
        {
            if (!_ActiveBehaviours.ContainsKey(name)) _ActiveBehaviours.Add(name, new InputBehaviour());
            _ActiveBehaviours[name].SetState(behaviourState);
        }

        public InputBehaviour GetBehaviour(String name)
        {
            return !_ActiveBehaviours.ContainsKey(name) ? null : _ActiveBehaviours[name];
        }
    }
}
