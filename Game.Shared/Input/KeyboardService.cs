﻿using System;
using System.Collections.Generic;
using AmosShared.Base;
using AmosShared.Events;
using AmosShared.Interfaces;
using AmosShared.Service;
using OpenTK.Input;

namespace Game.Shared.Input
{
    internal class KeyboardService : AService<KeyboardService>, IUpdatable
    {
        /// <summary>Event triggered upon a key first being pressed, data passed is the <see cref="Key"/> of the registered key</summary>
        public static String OnKeyPress = "OnKeyPress";
        /// <summary>Event triggered upon a key first being triggered for held, <see cref="HeldTime"/> after being initially pressed.
        /// Data passed is <see cref="KeyData"/> for the current key just held</summary>
        public static String OnKeyHeld = "OnKeyHeld";
        /// <summary>Event triggerd after initial <see cref="OnKeyHeld"/> and for every update until this key is <see cref="OnKeyReleased"/>. 
        /// Data passed is the <see cref="KeyData"/> of the current key being pressed</summary>
        public static String OnKeyHeldUpdate = "OnKeyHeldUpdate";
        /// <summary>Event triggered after the key has been released, data passed is the <see cref="Key"/> of the registered key</summary>
        public static String OnKeyReleased = "OnKeyReleased";

        /// <summary>All possible <see cref="Key"/>'s being used by this service</summary>
        private static Key[] _RegisteredKeys;

        /// <summary>Whether this object has had <see cref="Dispose"/> called</summary>
        public Boolean IsDisposed { get; set; }

        /// <summary>Register all possible <see cref="Key"/>'s to be used.  
        /// If <paramref name="debug"/> is true, then it will always output information about the current Keys being interacted with.
        /// Creates a lot of output logs.</summary>
        /// <param name="debug"></param>
        public void Initialise(Boolean debug = false)
        {
            List<Key> temp = new List<Key>();
            for (Int32 k = 0; k < (Int32)Key.LastKey; k++)
            {
                Key key = (Key)k;
                temp.Add(key);
                _KeysPressed.Add(key, new KeyData(TimeSpan.Zero, false, key));
            }
            _RegisteredKeys = temp.ToArray();

            UpdateManager.Instance.AddUpdatable(Instance);

            if (!debug) return;

            EventManager.Instance.StartListen(OnKeyPress, s => { Console.WriteLine($"{s} pressed"); });
            EventManager.Instance.StartListen(OnKeyHeld, s => { Console.WriteLine($"{s} initial held"); });
            EventManager.Instance.StartListen(OnKeyHeldUpdate, s =>
            {
                Console.WriteLine($"{((KeyData)s[0]).Key} held update for {((KeyData)s[0]).HeldTime.TotalSeconds:0.00} seconds(s) -> delta provided {((KeyData)s[0]).Delta.TotalMilliseconds:0.00} ms");
            });
            EventManager.Instance.StartListen(OnKeyReleased, s => { Console.WriteLine($"{s} released"); });
        }

        /// <summary>The previous state that the <see cref="Keyboard"/> was in last frame</summary>
        private static KeyboardState _PreviousState;
        /// <summary>The current state the <see cref="Keyboard"/> is in this frame</summary>
        private static KeyboardState _State;
        /// <summary>Keys currently being pressed</summary>
        private static readonly Dictionary<Key, KeyData> _KeysPressed = new Dictionary<Key, KeyData>();

        /// <summary>Whether or not the given key is pressing beind "Pressed" down</summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Boolean IsKeyDown(Key key) => _State.IsKeyDown(key);

        public override void Update()
        {
            base.Update();
            _State = Keyboard.GetState();

            foreach (var key in _RegisteredKeys)
            {
                KeyData currentKey = _KeysPressed[key];

                //If the key is down, and wasn't before. AND it isn't already registered as being down, then add it to the _KeysPressed and fire its event
                if (_State.IsKeyDown(key) && !_PreviousState.IsKeyDown(key) && !currentKey.Pressed && !currentKey.Held)
                {
                    currentKey.Pressed = true;
                    EventManager.Instance.Trigger(OnKeyPress, key);
                    //You have pressed the key, you can't release it in the same frame, so continue to the next key
                    continue;
                }

                //If the key currently isn't being tracked, then ignore it
                if (!currentKey.Pressed && !currentKey.Held) continue;

                //If this current key is being pressed, then increment the timer for how long it has been pressed for
                currentKey.IncreaseHeldTime();

                //If the current key is being held, then tell the world
                if (currentKey.Held)
                {
                    EventManager.Instance.Trigger(OnKeyHeldUpdate, currentKey);
                }
                //If the key as just been presed, then now its been held!
                else if (currentKey.Pressed)
                {
                    currentKey.Pressed = false;
                    currentKey.Held = true;
                    EventManager.Instance.Trigger(OnKeyHeld, key);
                }

                //If the key was previously down, but now it isnt. Its been released
                if (_PreviousState.IsKeyDown(key) && !_State.IsKeyDown(key))
                {
                    currentKey.Reset();
                    EventManager.Instance.Trigger(OnKeyReleased, key);
                }
            }

            _PreviousState = _State;
        }

        public Boolean CanUpdate() => !IsDisposed;

        public void Dispose()
        {
            IsDisposed = true;
            UpdateManager.Instance.RemoveUpdatable(Instance);
        }
    }
}
