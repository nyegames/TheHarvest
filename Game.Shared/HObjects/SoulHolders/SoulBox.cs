﻿using System;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using AmosShared.Interfaces;
using Game.Desktop.Glide;
using Game.Desktop.Health;
using Game.Desktop.Interfaces;
using Game.Desktop.Services;
using Game.Desktop.Services.Attackables;
using Game.Desktop.Services.Canvas;
using OpenTK;

namespace Game.Desktop.HObjects.SoulHolders
{
    public class SoulBox : HObject, IPositionable, IAttackable
    {
        private static readonly Vector4 _SoulColour = new Vector4(118f / 255f, 224f / 255f, 180f / 255f, 1f);

        private readonly Tweener _Tweener = new Tweener(false);

        private readonly HealthComponent _Health;
        private readonly Sprite _Sprite;

        private Vector2 _Position;
        public Vector2 Position
        {
            get => _Position;
            set
            {
                _Position = value;
                _Sprite.Position = value;
            }
        }

        public override Boolean Visible
        {
            get => base.Visible;
            set
            {
                base.Visible = value;
                _Sprite.Visible = value;
            }
        }

        public Boolean Active { get; set; }

        public Vector2 CenterPosition => _Sprite.Position - _Sprite.Offset + _Sprite.Size / 2;

        public Single Radius => _Sprite.Size.Length;

        public SoulBox(String name = null) : base(name)
        {
            _Sprite = new Sprite(CanvasService.GameCanvas, ZService.Instance["Soul_Boxs"], Texture.GetPixel())
            {
                Size = new Vector2(32, 50),
                Colour = _SoulColour
            };

            _Health = new HealthComponent(1);
            Active = _Health.Current > 0;

            AttackService.Instance.Register(this);
        }

        public void Attacked(IAttacker attacker)
        {
            _Health.RemoveHealth(1);
            var black = new Vector4(0, 0, 0, 1);
            var col = Vector4.Lerp(_SoulColour, black, 1 - ((Single)_Health.Current / _Health.Max));
            col = new Vector4(col.X, col.Y, col.Z, 1f);
            _Sprite.Colour = col;

            if (Active = _Health.Current > 0) return;

            col = new Vector4(col.X, col.Y, col.Z, 0f);
            var dir = (CenterPosition - attacker.CenterPosition).Normalized();
            dir = new Vector2(dir.X, Math.Abs(dir.Y));

            var endPos = _Sprite.Position + (dir * Radius * 2);
            _Tweener.Tween(_Sprite, new { Scale = new Vector2(2, 2), Colour = col, Position = endPos }, TimeSpan.FromSeconds(1))
            .OnComplete(() =>
            {
                Visible = false;
            });
        }

        public override void Update()
        {
            base.Update();
            _Tweener?.Update();
        }

        public override void Dispose()
        {
            base.Dispose();
            AttackService.Instance.UnRegister(this);
            _Sprite.Dispose();
        }
    }
}
