﻿using System;
using System.Collections.Generic;
using OpenTK;

namespace Game.Desktop.HObjects.SoulHolders.Handlers
{
    public class SoulBoxHandler : ObjectHandler
    {
        public SoulBoxHandler() : base("SoulBox")
        {
        }

        protected override SceneObject Create(Dictionary<String, Object> data)
        {
            var name = FromData<String>("Name", data) ?? HObject.GenerateName(typeof(SoulBox));
            var lvl = new SoulBox(name)
            {
                Position = FromData<Vector2>("Position", data),
                Visible = FromData<Boolean>("Visible", data)
            };
            return new SceneObject(name, lvl);
        }
    }
}
