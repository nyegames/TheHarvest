﻿using System;
using System.Collections.Generic;
using AmosShared.Interfaces;
using Game.Desktop.Interfaces.Core;

namespace Game.Desktop.HObjects
{
    public class HObject : IVisible, IUpdatable, IDisposable
    {
        public static String GenerateName(Type type) => $"{type}-{Guid.NewGuid()}";

        private static readonly Dictionary<String, List<HObject>> _HObjects = new Dictionary<String, List<HObject>>();

        public String Name { get; }

        public virtual Boolean Visible { get; set; }
        public Boolean IsDisposed { get; set; }

        public HObject(String name = null)
        {
            Name = name ?? GenerateName(GetType());
            if (!_HObjects.ContainsKey(Name)) _HObjects.Add(Name, new List<HObject>());
            _HObjects[Name].Add(this);
        }

        public virtual Boolean CanUpdate() => !IsDisposed;
        public virtual void Update()
        {

        }

        public virtual void Dispose()
        {
            _HObjects[Name].Remove(this);
            IsDisposed = true;
        }

        public override String ToString() => Name;
    }
}
