﻿using System;
using System.Collections.Generic;
using Game.Desktop.HObjects.TileMap;
using OpenTK;

namespace Game.Desktop.HObjects.SoulVessels.Handlers
{
    public class SoulVesselHandler : ObjectHandler
    {
        public SoulVesselHandler() : base("SoulVessel")
        {

        }

        protected override SceneObject Create(Dictionary<String, Object> data)
        {
            var name = FromData<String>("Name", data) ?? HObject.GenerateName(typeof(SceneObject));

            var pos = FromData<Vector2>("Position", data);
            var size = FromData<Vector2>("Size", data);

            //Position converted from top left, to bottom left + always increment the tile height.
            pos += new Vector2(0, -size.Y + HTileMap.TileSize);

            return new SceneObject(name, new SoulVessel(name)
            {
                Position = pos,
                Size = size,
                Visible = FromData<Boolean>("Visible", data)
            });
        }
    }
}
