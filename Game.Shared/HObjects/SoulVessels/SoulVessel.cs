﻿using System;
using AmosShared.Events;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using AmosShared.Interfaces;
using Game.Desktop.Glide;
using Game.Desktop.Interfaces;
using Game.Desktop.Services;
using Game.Desktop.Services.Canvas;
using Game.Desktop.Services.Interactions;
using Game.Desktop.Services.Level;
using OpenTK;

namespace Game.Desktop.HObjects.SoulVessels
{
    /// <summary>A Vessel is an interactable item which you are required to interact with in order to complete the level.  
    /// You will be required to interact with all Vessel's in a level to be able to complete it</summary>
    public class SoulVessel : HObject, IInteractable, ISizable
    {
        private Boolean _Collected;

        private readonly Sprite _Sprite;

        public Vector2 CenterPosition => Position + new Vector2(0, Size.Y / 2);

        public Single Radius => Size.X * 2;

        private Vector2 _Position;
        public Vector2 Position
        {
            get => _Position;
            set
            {
                _Position = value;
                _Sprite.Position = value;
            }
        }

        private Vector2 _Size;
        public Vector2 Size
        {
            get => _Size;
            set
            {
                _Size = value;
                _Sprite.Size = value;
                _Sprite.Offset = new Vector2(value.X / 2, 0);
            }
        }

        public override Boolean Visible
        {
            get => base.Visible;
            set
            {
                base.Visible = value;
                _Sprite.Visible = value;
            }
        }

        public Boolean Active { get; set; } = true;

        public SoulVessel(String name) : base(name)
        {
            _Sprite = new Sprite(CanvasService.GameCanvas, ZService.Instance.Get("Soul_Fragments"), Texture.GetPixel())
            {
                Colour = new Vector4(255f / 255f, 76f / 255f, 91f / 255f, 0.7f),
            };
            _Sprite.ScaleOrigin = _Sprite.Offset;
            _Sprite.RotationOrigin = _Sprite.Offset;

            Tweener.GlobalTweener.Tween(_Sprite, new { Scale = new OpenTK.Vector2(0.95f, 0.95f) }, TimeSpan.FromSeconds(2)).Reflect().Repeat().Ease(Ease.BackInOut);

            InteractService.Instance.Register(this);
        }

        public void Interacted(IInteractor interactor)
        {
            _Collected = true;
            InteractService.Instance.UnRegister(this);

            Tweener.GlobalTweener.TargetCancel(_Sprite);

            Tweener.GlobalTweener.Tween(_Sprite, new { Colour = new OpenTK.Vector4(0, 0, 0, _Sprite.Colour.W) }, TimeSpan.FromSeconds(0.3f))
            .OnComplete(() =>
            {
                Tweener.GlobalTweener.Tween(_Sprite, new { Scale = new OpenTK.Vector2(0, 0), Colour = new OpenTK.Vector4(0, 0, 0, 0) }, TimeSpan.FromSeconds(2))
                .OnComplete(() =>
                {
                    Visible = false;
                });
            });

            LevelService.Instance.CollectSoulVessel(Name);
        }

        public override void Dispose()
        {
            Tweener.GlobalTweener.TargetCancel(_Sprite);

            _Sprite.Dispose();

            base.Dispose();

            if (_Collected) return;
            InteractService.Instance.UnRegister(this);
        }

        public String InteractInstruction()
        {
            return "Release";
        }
    }
}
