﻿using System;
using AmosShared.Events;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using AmosShared.Interfaces;
using Game.Desktop.Glide;
using Game.Desktop.Interfaces;
using Game.Desktop.Services;
using Game.Desktop.Services.Canvas;
using Game.Desktop.Services.Interactions;
using Game.Desktop.Services.Level;
using OpenTK;

namespace Game.Desktop.HObjects.EndPortals
{
    /// <summary></summary>
    public class EndPortal : HObject, IInteractable, ISizable
    {
        public const String OnPortalEntered = "EndPortal-PortalEntered";

        /// <summary>The archway surrounding the portal</summary>
        private readonly Sprite _PortalArch;
        /// <summary>Sprite used for visual of the portal</summary>
        private readonly Sprite _PortalFluid;
        /// <summary>Backing field of the position</summary>
        private Vector2 _Position;

        /// <summary>The proportional value of the current progression between collecting all of the soul vessels in the scene</summary>
        private Single _CollectedProportion;

        private Single _VisuallyCollectedProportion;

        public Vector2 Position
        {
            get => _Position;
            set
            {
                _Position = value;
                _PortalArch.Position = value;
                _PortalFluid.Position = value + new Vector2(5, 0);
            }
        }

        private Vector2 _Size;

        public Vector2 Size
        {
            get => _Size;
            set
            {
                _Size = value;
                _PortalArch.Size = value;
                _PortalFluid.Size = value + new Vector2(-10, -5);
            }
        }

        public override Boolean Visible
        {
            get => base.Visible;
            set
            {
                base.Visible = value;
                _PortalFluid.Visible = value;
                _PortalArch.Visible = value;
            }
        }

        public Boolean Active { get; set; } = true;

        public Vector2 CenterPosition => Position + Size / 2;

        public Single Radius => Size.X * 2;

        public EndPortal(String name = null) : base(name)
        {
            _PortalArch = new Sprite(CanvasService.GameCanvas, ZService.Instance.Get("Portal_Arch"), Texture.GetPixel())
            {
                Size = new Vector2(20, 15),
                Colour = new Vector4(0.3f, 0.3f, 0.3f, 1f)
            };

            _PortalFluid = new Sprite(CanvasService.GameCanvas, ZService.Instance.Get("Portal_Liquid"), Texture.GetPixel())
            {
                Size = new Vector2(10, 5),
                Colour = new Vector4(53f / 255f, 255f / 255f, 154f / 255f, 0.7f),
                Scale = new Vector2(1, 0),
            };

            InteractService.Instance.Register(this);

            Active = false;

            EventManager.Instance.StartListen(LevelService.LevelProgressUpdated, VesselCollected);
            EventManager.Instance.StartListen(LevelService.LevelConditionsMet, AllVesselsCollected);
        }

        private void VesselCollected(Object[] data)
        {
            Int32 count = (Int32)data[0];
            Int32 total = (Int32)data[1];
            _CollectedProportion = count / (Single)total;

            Single seconds = _VisuallyCollectedProportion >= 1f ? 0 : 2f;

            Tweener.GlobalTweener.TargetCancel(_PortalFluid);
            var scale = 1f * _CollectedProportion;

            Tweener.GlobalTweener.Tween(_PortalFluid, new { Scale = new Vector2(1f, scale) }, TimeSpan.FromSeconds(seconds))
            .Ease(Ease.CubeOut)
            .OnComplete(() =>
            {
                if (_VisuallyCollectedProportion >= 1f) return;
                _VisuallyCollectedProportion = _CollectedProportion;
            });
        }

        private void AllVesselsCollected(Object[] data)
        {
            _CollectedProportion = 1f;
            Active = true;
        }

        public String InteractInstruction()
        {
            if (_VisuallyCollectedProportion >= 1) return "Enter";
            return "Replenish";
        }

        public void Interacted(IInteractor interactor)
        {
            Active = false;
            EventManager.Instance.Trigger(OnPortalEntered, Name);
        }

        public override void Dispose()
        {
            base.Dispose();
            _PortalArch.Dispose();
            _PortalFluid.Dispose();

            InteractService.Instance.UnRegister(this);

            EventManager.Instance.StopListen(LevelService.LevelProgressUpdated, VesselCollected);
            EventManager.Instance.StopListen(LevelService.LevelConditionsMet, AllVesselsCollected);
        }
    }
}
