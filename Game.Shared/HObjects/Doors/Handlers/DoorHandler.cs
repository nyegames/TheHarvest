﻿using System;
using System.Collections.Generic;
using Game.Desktop.Services;
using OpenTK;

namespace Game.Desktop.HObjects.Doors.Handlers
{
    public class DoorHandler : ObjectHandler
    {
        public DoorHandler() : base("Door") { }

        protected override SceneObject Create(Dictionary<String, Object> data)
        {
            var door = new Door(
                FromData<Vector2>("Size", data),
                ZService.Instance[FromData<String>("Z", data)],
                FromData<String>("Name", data));

            return new SceneObject(door.Name, door);
        }
    }
}
