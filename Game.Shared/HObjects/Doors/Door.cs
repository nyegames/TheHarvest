﻿using System;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using Game.Desktop.Interfaces;
using Game.Desktop.Services.Canvas;
using Game.Desktop.Services.Interactions;
using OpenTK;

namespace Game.Desktop.HObjects.Doors
{
    public class Door : HObject, IInteractable, ICollidable
    {
        private readonly Sprite[] _OpenSprites;
        private readonly Sprite[] _CloseSprites;

        public Vector2 CenterPosition => Position + Size / 2;

        public Single Radius => Math.Max(50, Size.X * 1.5f);

        //Bottom left position
        private Vector2 _Position;
        public Vector2 Position
        {
            get => _Position;
            set
            {
                _Position = value;
                for (Int32 i = 0; i < _OpenSprites.Length; i++)
                {
                    var s = _OpenSprites[i];
                    s.Position = value + new Vector2(0, i == 0 ? 0 : Size.Y / 2);
                }
                for (Int32 i = 0; i < _CloseSprites.Length; i++)
                {
                    var s = _CloseSprites[i];
                    s.Position = value + new Vector2(0, i == 0 ? 0 : Size.Y / 2);
                }
            }
        }

        public override Boolean Visible
        {
            set
            {
                base.Visible = value;
                foreach (var s in _OpenSprites) s.Visible = value && Open;
                foreach (var s in _CloseSprites) s.Visible = value && !Open;
            }
        }

        private readonly Vector2 _Size;
        public Vector2 Size
        {
            get => _Size;
            set => throw new NotImplementedException();
        }

        public Boolean Active { get; set; } = true;

        public Boolean Open { get; private set; }

        public Door(Vector2 size, Int32 zOrder, String name = null) : base(name)
        {
            _Size = size;

            String platformPath = "Content/Packed/Graphics/extras/platformPack_tile";

            _OpenSprites = new Sprite[2];
            for (Int32 i = 0; i < 2; i++)
            {
                Int32 key = i == 0 ? 57 : 48;
                _OpenSprites[i] = new Sprite(CanvasService.GameCanvas, zOrder, Texture.GetTexture($"{platformPath}{key:000}.png"))
                {
                    Size = new Vector2(size.X, size.Y / 2)
                };
            }

            _CloseSprites = new Sprite[2];
            for (Int32 i = 0; i < 2; i++)
            {
                Int32 key = i == 0 ? 58 : 49;
                _CloseSprites[i] = new Sprite(CanvasService.GameCanvas, zOrder, Texture.GetTexture($"{platformPath}{key:000}.png"))
                {
                    Size = new Vector2(size.X, size.Y / 2)
                };
            }

            InteractService.Instance.Register(this);

            Visible = true;
        }

        public void Interacted(IInteractor interactor)
        {
            Open = !Open;
            Visible = Visible;
        }

        public String InteractInstruction()
        {
            return Open ? "Close" : "Open";
        }

        public Boolean Collide(Vector2 bPos, Vector2 bSize)
        {
            if (Open) return false;
            return CollideUtils.AABB(this, bPos, bSize);
        }

        public override void Dispose()
        {
            base.Dispose();
            foreach (var s in _OpenSprites) s.Dispose();
            foreach (var s in _CloseSprites) s.Dispose();
            InteractService.Instance.UnRegister(this);
        }
    }
}
