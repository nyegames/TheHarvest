﻿using System;
using System.Linq;
using AmosShared.Events;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using AmosShared.Interfaces;
using Game.Desktop.Glide;
using Game.Desktop.Interfaces;
using Game.Desktop.Services;
using Game.Desktop.Services.Canvas;
using Game.Desktop.Services.Character;
using Game.Desktop.Services.Interactions;
using Game.Desktop.Services.Scenes;
using OpenTK;

namespace Game.Desktop.HObjects.LevelSelectors
{
    /// <summary>This object will load a Scene that has been assigned to it when the character interacts with it, twice in a row</summary>
    public class LevelSelector : HObject, IInteractable, IPositionable, ISizable
    {
        protected Sprite _Sprite;

        protected Tweener _Tweener = new Tweener(false);

        public override Boolean Visible
        {
            get => base.Visible;
            set
            {
                base.Visible = value;
                _Sprite.Visible = value;
            }
        }

        private Vector2 _Position;
        public Vector2 Position
        {
            get => _Position;
            set
            {
                _Position = value;
                _Sprite.Position = value;
            }
        }

        private Vector2 _Size;

        public Vector2 Size
        {
            get => _Size;
            set
            {
                _Size = value;
                _Sprite.Size = value;
            }
        }

        public Boolean Active { get; set; } = true;

        public Vector2 CenterPosition => _Position + new Vector2(0, Size.Y * _Sprite.Scale.Y / 2);

        public Single Radius => Size.X;

        public LevelSelector(String z, Vector2 pos, Vector2 size, String name = null) : base(name)
        {
            _Position = pos;
            _Size = size;

            _Sprite = new Sprite(CanvasService.GameCanvas, ZService.Instance[z], Texture.GetPixel())
            {
                Size = size,
                Position = pos,
                Scale = new Vector2(0.5f, 0.5f),
                Offset = new Vector2(size.X / 2, 0f),
                ScaleOrigin = new Vector2(size.X / 2, 0f)
            };

            InteractService.Instance.Register(this);
        }

        private void SetActive(Boolean active)
        {
            if (Active == active) return;
            Active = active;

            _Tweener.TargetCancel(_Sprite);

            if (Active)
            {
                _Tweener.Tween(_Sprite, new { Scale = new Vector2(1, 1) }, TimeSpan.FromSeconds(1)).Ease(Ease.BackOut);
            }
            else
            {
                _Tweener.Tween(_Sprite, new { Scale = new Vector2(0.5f, 0.5f) }, TimeSpan.FromSeconds(1)).Ease(Ease.BackInOut);
            }
        }

        public override void Update()
        {
            base.Update();
            if (CharacterService.Instance.ActiveCharacter == null) return;

            _Tweener.Update();

            Boolean active = Math.Abs((CharacterService.Instance.ActiveCharacter.CenterPosition - CenterPosition).Length) < Radius;

            SetActive(active);
        }

        public void Interacted(IInteractor interactor)
        {
            SceneService.Instance.LoadNextLevel();
        }

        public String InteractInstruction()
        {
            return "Load Level";
        }

        public override void Dispose()
        {
            base.Dispose();
            _Sprite.Dispose();
            InteractService.Instance.UnRegister(this);
        }
    }
}
