﻿using System;
using System.Collections.Generic;
using Game.Desktop.HObjects.TileMap;
using OpenTK;

namespace Game.Desktop.HObjects.LevelSelectors.Handlers
{
    public class LevelSelectorHandler : ObjectHandler
    {
        public LevelSelectorHandler() : base("LevelSelector")
        {

        }

        protected override SceneObject Create(Dictionary<String, Object> data)
        {
            var name = FromData<String>("Name", data) ?? HObject.GenerateName(typeof(LevelSelector));

            var pos = FromData<Vector2>("Position", data);
            var size = FromData<Vector2>("Size", data);

            //Position converted from top left, to bottom left + always increment the tile height.
            pos += new Vector2(0, -size.Y + HTileMap.TileSize);

            var lvl = new LevelSelector("Background", pos, size, name)
            {
                Visible = FromData<Boolean>("Visible", data)
            };
            return new SceneObject(name, lvl);
        }
    }
}
