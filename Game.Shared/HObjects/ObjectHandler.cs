﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AmosShared.Interfaces;
using Game.Desktop.Interfaces.Core;
using Game.Desktop.Scenes;
using OpenTK;

namespace Game.Desktop.HObjects
{
    /// <summary>An object which is part of a <see cref="HScene"/> which requires an <see cref="Object"/>
    /// and a <see cref="String"/> Name</summary>
    public class SceneObject
    {
        public String Name { get; private set; }
        public Object Object { get; private set; }

        public SceneObject(String name, Object obj)
        {
            Name = name;
            Object = obj;
        }
    }

    public abstract class ObjectHandler
    {
        protected String _TypeName { get; }

        protected ObjectHandler(String typeName)
        {
            _TypeName = typeName;
        }

        public virtual SceneObject Create(String typeName, Dictionary<String, Object> data)
        {
            SceneObject obj = null;
            obj = _TypeName.Equals(typeName) ? Create(data) : null;

            return obj;
        }

        protected T FromData<T>(String name, Dictionary<String, Object> data)
        {
            if (data[name] is T f) return f;
            return default(T);
        }

        protected abstract SceneObject Create(Dictionary<String, Object> data);
    }
}
