﻿using System;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using AmosShared.Interfaces;
using Game.Desktop.Characters;
using Game.Desktop.Glide;
using Game.Desktop.Interfaces;
using Game.Desktop.Services;
using Game.Desktop.Services.Canvas;
using Game.Desktop.Services.Character;
using Game.Desktop.Services.Collectibles;
using OpenTK;

namespace Game.Desktop.HObjects.SoulFragment
{
    public class SoulFragment : HObject, ICollectible, IPositionable, ISizable
    {
        private readonly Tweener _Tweener = new Tweener(false);

        public Vector2 CenterPosition => Position;

        public Single Radius { get; }

        public Vector2 Size { get; set; }

        private Vector2 _Position;
        public Vector2 Position
        {
            get => _Position;
            set
            {
                _Position = value;
                _Sprite.Position = value;
            }
        }

        public override Boolean Visible
        {
            get => base.Visible;
            set
            {
                base.Visible = value;
                _Sprite.Visible = value;
            }
        }

        public Boolean Active { get; set; } = true;

        private readonly Sprite _Sprite;
        protected Boolean _Collected;

        protected Character _CollectedBy;

        public SoulFragment(String name = null) : base(name)
        {
            Size = new Vector2(20, 20);
            Radius = 50;

            _Sprite = new Sprite(CanvasService.GameCanvas, ZService.Instance.Get("Soul_Fragments"), Texture.GetPixel())
            {
                Size = Size,
                Colour = new Vector4(0.2323323f, 0.4535f, 0.5666f, 0.7f),
            };
            _Sprite.Offset = _Sprite.Size / 2;
            _Sprite.ScaleOrigin = _Sprite.Size / 2;
            _Sprite.RotationOrigin = _Sprite.Size / 2;

            _Tweener.Tween(_Sprite, new { Rotation = Math.PI * 2 }, TimeSpan.FromSeconds(5)).Repeat();
            _Tweener.Tween(_Sprite, new { Scale = new Vector2(0.8f, 0.8f) }, TimeSpan.FromSeconds(2)).Reflect().Repeat();

            CollectService.Instance.Register(this);
        }

        public void Collect()
        {
            CollectService.Instance.Collected(this);
            _Collected = true;
            _Tweener.TargetCancel(_Sprite);

            _Tweener.Tween(_Sprite, new { Rotation = -Math.PI * 2 }, TimeSpan.FromSeconds(0.3));

            _Tweener.Tween(_Sprite, new { Scale = Vector2.One * 1.3f }, TimeSpan.FromSeconds(0.2)).Ease(Ease.BackOut)
                .OnComplete(() =>
            {
                _Tweener.Tween(_Sprite, new { Scale = Vector2.Zero }, TimeSpan.FromSeconds(0.12))
                .OnComplete(() =>
                {
                    Visible = false;
                });
            });
        }

        public override void Update()
        {
            base.Update();
            _Tweener.Update();
            if (_Collected) return;

            var character = CharacterService.Instance.ActiveCharacter;
            var dist = Math.Abs((character.CenterPosition - CenterPosition).Length);
            if (dist > Radius) return;
            _CollectedBy = character;
            Collect();
        }

        public override void Dispose()
        {
            base.Dispose();
            _Sprite.Dispose();
            CollectService.Instance.UnRegister(this);
        }
    }
}