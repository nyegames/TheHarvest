﻿using System;
using System.Collections.Generic;
using OpenTK;

namespace Game.Desktop.HObjects.SoulFragment.Handlers
{
    public class SoulFragmentHandler : ObjectHandler
    {
        public SoulFragmentHandler() : base("SoulFragment")
        {
        }

        protected override SceneObject Create(Dictionary<String, Object> data)
        {
            String name = FromData<String>("Name", data) ?? HObject.GenerateName(typeof(SoulFragment));
            SoulFragment fragment = new SoulFragment(name)
            {
                Position = FromData<Vector2>("Position", data),
                Visible = FromData<Boolean>("Visible", data)
            };
            return new SceneObject(name, fragment);
        }
    }
}
