﻿using System;
using AmosShared.Interfaces;
using OpenTK;

namespace Game.Desktop.HObjects.Spawn
{
    public class SpawnPosition : HObject, IPositionable
    {
        public Vector2 Position { get; set; }

        public SpawnPosition(String name = null) : base(name) { }
    }
}
