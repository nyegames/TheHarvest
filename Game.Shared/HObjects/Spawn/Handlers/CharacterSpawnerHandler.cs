﻿using System;
using System.Collections.Generic;
using Game.Desktop.HObjects.TileMap;
using OpenTK;

namespace Game.Desktop.HObjects.Spawn.Handlers
{
    public class CharacterSpawnerHandler : ObjectHandler
    {
        public CharacterSpawnerHandler() : base("SpawnPosition") { }

        protected override SceneObject Create(Dictionary<String, Object> data)
        {
            var name = FromData<String>("Name", data) ?? HObject.GenerateName(typeof(SpawnPosition));
            var pos = FromData<Vector2>("Position", data);
            var size = FromData<Vector2>("Size", data);

            //Position converted from top left, to bottom left + always increment the tile height.
            pos += new Vector2(0, -size.Y + HTileMap.TileSize);

            return new SceneObject(name, new SpawnPosition(name)
            {
                Position = pos + new Vector2(size.X / 4, 0f)
            });
        }
    }
}
