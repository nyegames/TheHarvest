﻿using System;
using AmosShared.Graphics;
using Game.Desktop.HObjects.TileMap.Tile;
using OpenTK;

namespace Game.Desktop.HObjects.TileMap.Handlers
{
    public class PlatformTileHandler : HTileHandler
    {
        internal override HTile Create(Int64 key, Int64 gridX, Int64 gridY, Vector2 tileSize)
        {
            String platformPath = "Content/Packed/Graphics/tiles/platformPack_tile";

            if (key < 0) return null;

            var tile = new HTile(Texture.GetTexture($"{platformPath}{key:000}.png"), "Tile_Back", tileSize, gridX, gridY)
            {
                Friction = 1f,
                SideFriction = 0.5f
            };

            switch (key)
            {
                case 10://Ice
                    tile.Friction = 0.34f;
                    break;
                //Slow side slide
                case 17:
                case 3:
                    tile.SideFriction = 0.85f;
                    break;
                //Stick side slide
                case 15:
                    tile.SideFriction = 1;
                    break;
            }

            return tile;
        }
    }
}
