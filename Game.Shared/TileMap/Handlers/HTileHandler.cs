﻿using System;
using Game.Desktop.HObjects.TileMap.Tile;

namespace Game.Desktop.HObjects.TileMap.Handlers
{
    public abstract class HTileHandler
    {
        internal abstract HTile Create(Int64 key, Int64 gridX, Int64 gridY, OpenTK.Vector2 tileSize);
    }
}
