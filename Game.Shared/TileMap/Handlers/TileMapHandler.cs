﻿using System;
using System.Collections.Generic;
using System.IO;
using Game.Desktop.HObjects.TileMap.Tile;
using Game.Desktop.Parsers;

namespace Game.Desktop.HObjects.TileMap.Handlers
{
    class TileMapHandler : ObjectHandler
    {
        public TileMapHandler() : base("TileMap")
        {
        }

        protected override SceneObject Create(Dictionary<String, Object> data)
        {
            String pathToTileMap = FromData<String>("Source", data);

            var entry = System.Reflection.Assembly.GetEntryAssembly();
            String folder = $"{entry.Location}".TrimEnd(entry.ManifestModule.Name.ToCharArray());
            String filePath = $"{folder}\\{pathToTileMap}";

            String[] readLines = File.ReadAllLines(filePath);

            String[][] lines = new String[readLines.Length][];
            for (Int32 i = 0; i < lines.Length; i++) lines[i] = readLines[i].Split(',');

            Int32 tileSize = (Int32)FromData<Single>("TileSize", data);
            Int32 yMax = lines.Length;
            Int32 xMax = lines[0].Length;

            Int32[][] keys = new Int32[yMax][];
            HTile[][] tiles = new HTile[yMax][];

            var parser = new HTileParser();
            OpenTK.Vector2 size = new OpenTK.Vector2(tileSize, tileSize);

            for (Int32 y = 0; y < yMax; y++)
            {
                keys[y] = new Int32[xMax];
                tiles[y] = new HTile[xMax];

                for (Int32 x = 0; x < xMax; x++)
                {
                    keys[y][x] = Int32.Parse(lines[y][x]);

                    tiles[y][x] = parser.Create(keys[y][x], x, y, size);
                    tiles[y][x].Position = new OpenTK.Vector2(size.X * x, -size.Y * y);
                }
            }

            HTileMap tileMap = new HTileMap(tiles, tileSize);
            return new SceneObject(FromData<String>("Name", data), tileMap);
        }
    }
}
