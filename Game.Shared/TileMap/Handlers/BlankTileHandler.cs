﻿using System;
using Game.Desktop.HObjects.TileMap.Tile;
using OpenTK;

namespace Game.Desktop.HObjects.TileMap.Handlers
{
    class BlankTileHandler : HTileHandler
    {
        internal override HTile Create(Int64 key, Int64 gridX, Int64 gridY, Vector2 tileSize)
        {
            if (key >= 0) return null;
            return new HTileOpen(null, "Tile_Back", tileSize, gridX, gridY);
        }
    }
}
