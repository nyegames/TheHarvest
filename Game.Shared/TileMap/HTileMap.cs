﻿using System;
using System.Collections.Generic;
using System.Linq;
using AmosShared.Interfaces;
using Game.Desktop.HObjects.TileMap.Tile;
using Game.Desktop.Interfaces;
using OpenTK;

namespace Game.Desktop.HObjects.TileMap
{
    public class HTileMap : ISizable, IDisposable
    {
        private readonly HTile[][] _Tiles;
        private readonly List<ICollidable> _Collidables = new List<ICollidable>();

        public static Int32 TileSize { get; private set; } = 32;

        public Vector2 Size
        {
            get => new Vector2(_Tiles.Length, _Tiles[0].Length);
            set => throw new NotSupportedException();
        }

        public String Name { get; private set; }

        private static Int32 _Count;

        /// <summary></summary>
        /// <param name="tiles"></param>
        /// <param name="tileSize"></param>
        public HTileMap(HTile[][] tiles, Int32 tileSize)
        {
            _Tiles = tiles;
            TileSize = tileSize;
            Name = $"{GetType()}_{_Count++}";
        }

        public void AddCollidable(ICollidable col)
        {
            _Collidables.Add(col);
        }

        public HTile[][] GetTiles()
        {
            HTile[][] tiles = _Tiles;
            for (Int32 x = 0; x < _Tiles.Length; x++)
            {
                tiles[x] = _Tiles[x].ToArray();
            }
            return tiles;
        }

        public HTile GetTile(Int32 x, Int32 y)
        {
            if (y < 0) y = 0;
            if (y >= _Tiles.Length) y = _Tiles.Length - 1;
            if (x < 0) x = 0;
            if (x >= _Tiles[y].Length) x = _Tiles[y].Length - 1;

            return _Tiles[y][x];
        }

        public Int32[] GetGridCoords(Vector2 position)
        {
            var gridX = (Int32)(((position.X + TileSize / 2) / TileSize));
            var gridY = (Int32)(((position.Y - TileSize / 2) / TileSize)) * -1;

            return new[] { gridX, gridY };
        }

        public Vector2 AttemptMovement(Vector2 pos, Vector2 size, Vector2 velocity)
        {
            List<ICollidable> tiles = new List<ICollidable>();
            var currentPosition = pos;
            var gridCoords = GetGridCoords(currentPosition);

            //Store the current position of the player using the X velocity
            var nextPos = currentPosition + new Vector2(0f, velocity.Y);
            currentPosition += CollisionDetection(nextPos, size, new Vector2(0f, velocity.Y), gridCoords);

            gridCoords = GetGridCoords(currentPosition);

            //Recreate the player rectangle with the newly moved position
            nextPos = currentPosition + new Vector2(velocity.X, 0f);

            //Attempt moving in the Y velocity
            currentPosition += CollisionDetection(nextPos, size, new Vector2(velocity.X, 0), gridCoords);

            return currentPosition;
        }

        public List<HTile> CollisionDetect(Vector2 position, Vector2 size)
        {
            var gridCoords = GetGridCoords(position);
            var tiles = GetBroadTiles(gridCoords[0], gridCoords[1], new Vector2(0, 0), size);

            List<HTile> collided = new List<HTile>();

            foreach (var tile in tiles)
            {
                if (tile is HTileOpen) continue;

                if (CollideUtils.AABB(position, size, tile.Position, tile.Size))
                {
                    collided.Add(tile);
                }
            }

            return collided;
        }

        private List<HTile> GetBroadTiles(Int32 gridX, Int32 gridY, Vector2 velocity, Vector2 playerSize)
        {
            List<HTile> tiles = new List<HTile>();

            //[0,0] [1,0] [2,0]
            //[0,1] [1,1] [2,1]
            //[0,2]*[1,2] [2,2]
            //[0,3] [1,3] [2,3]

            //Find all the grid squares which are located around the player

            for (Int32 i = -2; i < 2; i++)
            {
                //you are moving down, then don't check any squares above you
                if (i < 0 && velocity.Y < 0) continue;

                //If you are moving up, then don't check any squares below you
                if (i > 0 && velocity.Y > 0) continue;

                tiles.Add(GetTile(gridX - 1, gridY + i));
                tiles.Add(GetTile(gridX, gridY + i));
                tiles.Add(GetTile(gridX + 1, gridY + i));
            }

            return tiles;
        }

        /// <summary>Will return the allowed Velocity that the player can use</summary>
        /// <param name="playerRectangle"></param>
        /// <param name="velocity"></param>
        /// <param name="tiles"></param>
        /// <returns></returns>
        private Vector2 CollisionDetection(Vector2 playerPos, Vector2 playerSize, Vector2 velocity, Int32[] gridCoords)
        {
            Int32 gridX = gridCoords[0];
            Int32 gridY = gridCoords[1];
            List<ICollidable> collidables = GetBroadTiles(gridX, gridY, velocity, playerSize).OfType<ICollidable>().ToList();
            collidables.AddRange(_Collidables);

            //Move the player rectangle by the velocity provided
            Vector2 afterVelPos = playerPos + velocity;

            //Search all broad search tiles for collision
            foreach (var collidable in collidables)
            {
                //Will you collide with any broad phase checks if you moved the full velocity?
                if (collidable.Collide(afterVelPos, playerSize))
                {
                    //Revert the full velocity movement
                    var pos = playerPos - velocity;

                    //Break it down into steps
                    Int32 max = 50;
                    var step = velocity / max;
                    var vel = new Vector2(0, 0);

                    //Move through each step of velocity
                    for (Int32 i = 0; i < max; i++)
                    {
                        pos += step;

                        //If one of the steps causes a collision with a tile, revert the step and return the allowed velocity
                        if (collidable.Collide(pos, playerSize))
                        {
                            velocity = step * (i - 1);
                            pos -= step;

                            break;
                        }
                    }

                    break;
                }
            }

            return velocity;
        }

        public override String ToString()
        {
            return Name;
        }

        public void Dispose()
        {
            _Collidables.Clear();
            foreach (var tiles in _Tiles)
            {
                foreach (var tile in tiles)
                {
                    tile.Dispose();
                }
            }
        }
    }
}
