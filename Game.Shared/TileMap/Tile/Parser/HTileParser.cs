﻿using System;
using System.Collections.Generic;
using Game.Desktop.HObjects.TileMap.Handlers;
using Game.Desktop.HObjects.TileMap.Tile;

namespace Game.Desktop.Parsers
{
    public class HTileParser
    {
        protected IEnumerable<HTileHandler> _Handlers = new List<HTileHandler>()
        {
            new BlankTileHandler(),
            new PlatformTileHandler()
    };

        internal HTile Create(Int64 key, Int64 gridX, Int64 gridY, OpenTK.Vector2 tileSize)
        {
            foreach (var h in _Handlers)
            {
                var t = h.Create(key, gridX, gridY, tileSize);
                if (t != null) return t;
            }
            throw new Exception($"No Handler has been added to handle the key {key}");
        }
    }
}
