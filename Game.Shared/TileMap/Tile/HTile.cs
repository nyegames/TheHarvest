﻿using System;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using Game.Desktop.Interfaces;
using Game.Desktop.Services;
using Game.Desktop.Services.Canvas;
using OpenTK;

namespace Game.Desktop.HObjects.TileMap.Tile
{
    public class HTile : INameable, IDisposable, ICollidable
    {
        public String Name { get; }

        private Vector2 _Position;
        public Vector2 Position
        {
            get => _Position;
            set
            {
                _Position = value;
                if (Sprite != null) Sprite.Position = value;
            }
        }

        public Sprite Sprite { get; private set; }

        private readonly Vector2 _Size;
        public Vector2 Size
        {
            get => _Size;
            set => throw new NotSupportedException();
        }

        public Int64 GridX { get; private set; }

        public Int64 GridY { get; private set; }

        public Single Friction { get; internal set; } = 0.1f;

        public Single SideFriction { get; internal set; } = 0.1f;

        public HTile(Texture texture, String z, Vector2 size, Int64 x, Int64 y, String name = null)
        {
            _Size = size;
            Name = name ?? HObject.GenerateName(GetType());

            GridX = x;
            GridY = y;

            if (texture != null)
            {
                Sprite = new Sprite(CanvasService.GameCanvas, ZService.Instance[z], texture)
                {
                    Size = size,
                    Visible = true
                };
            }
        }

        public override String ToString()
        {
            return $"Tile[ {GridX},{GridY} ]";
        }

        public void Dispose()
        {
            Sprite?.Dispose();
        }

        public virtual Boolean Collide(Vector2 bPos, Vector2 bSize)
        {
            return CollideUtils.AABB(this, bPos, bSize);
        }
    }
}
