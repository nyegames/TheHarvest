﻿using System;
using AmosShared.Graphics;
using OpenTK;

namespace Game.Desktop.HObjects.TileMap.Tile
{
    class HTileOpen : HTile
    {
        public HTileOpen(Texture texture, String z, Vector2 size, Int64 x, Int64 y, String name = null)
            : base(texture, z, size, x, y, name)
        {
        }

        public override Boolean Collide(Vector2 bPos, Vector2 bSize)
        {
            return false;
        }
    }
}
