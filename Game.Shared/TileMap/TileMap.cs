﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Game.Desktop.Tiled;
//
//    var tileMap = TileMap.FromJson(jsonString);

namespace Game.Desktop.Tiled
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class TileMap
    {
        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("infinite")]
        public bool Infinite { get; set; }

        [JsonProperty("layers")]
        public Layer[] Layers { get; set; }

        [JsonProperty("nextobjectid")]
        public long Nextobjectid { get; set; }

        [JsonProperty("orientation")]
        public string Orientation { get; set; }

        [JsonProperty("renderorder")]
        public string Renderorder { get; set; }

        [JsonProperty("tiledversion")]
        public string Tiledversion { get; set; }

        [JsonProperty("tileheight")]
        public long Tileheight { get; set; }

        [JsonProperty("tilesets")]
        public Tileset[] Tilesets { get; set; }

        [JsonProperty("tilewidth")]
        public long Tilewidth { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("version")]
        public long Version { get; set; }

        [JsonProperty("width")]
        public long Width { get; set; }
    }

    public partial class Layer
    {
        [JsonProperty("data", NullValueHandling = NullValueHandling.Ignore)]
        public long[] Data { get; set; }

        [JsonProperty("height", NullValueHandling = NullValueHandling.Ignore)]
        public long? Height { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("opacity")]
        public long Opacity { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("visible")]
        public bool Visible { get; set; }

        [JsonProperty("width", NullValueHandling = NullValueHandling.Ignore)]
        public long? Width { get; set; }

        [JsonProperty("x")]
        public long X { get; set; }

        [JsonProperty("y")]
        public long Y { get; set; }

        [JsonProperty("draworder", NullValueHandling = NullValueHandling.Ignore)]
        public string Draworder { get; set; }

        [JsonProperty("objects", NullValueHandling = NullValueHandling.Ignore)]
        public Object[] Objects { get; set; }
    }

    public partial class Object
    {
        [JsonProperty("height")]
        public double Height { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("point", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Point { get; set; }

        [JsonProperty("rotation")]
        public long Rotation { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("visible")]
        public bool Visible { get; set; }

        [JsonProperty("width")]
        public double Width { get; set; }

        [JsonProperty("x")]
        public double X { get; set; }

        [JsonProperty("y")]
        public double Y { get; set; }

        [JsonProperty("ellipse", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Ellipse { get; set; }

        [JsonProperty("polygon", NullValueHandling = NullValueHandling.Ignore)]
        public Poly[] Polygon { get; set; }

        [JsonProperty("polyline", NullValueHandling = NullValueHandling.Ignore)]
        public Poly[] Polyline { get; set; }
    }

    public partial class Poly
    {
        [JsonProperty("x")]
        public double X { get; set; }

        [JsonProperty("y")]
        public double Y { get; set; }
    }

    public partial class Tileset
    {
        [JsonProperty("columns")]
        public long Columns { get; set; }

        [JsonProperty("firstgid")]
        public long Firstgid { get; set; }

        [JsonProperty("grid")]
        public Grid Grid { get; set; }

        [JsonProperty("margin")]
        public long Margin { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("spacing")]
        public long Spacing { get; set; }

        [JsonProperty("tilecount")]
        public long Tilecount { get; set; }

        [JsonProperty("tileheight")]
        public long Tileheight { get; set; }

        [JsonProperty("tiles")]
        public Dictionary<string, Tile> Tiles { get; set; }

        [JsonProperty("tilewidth")]
        public long Tilewidth { get; set; }
    }

    public partial class Grid
    {
        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("orientation")]
        public string Orientation { get; set; }

        [JsonProperty("width")]
        public long Width { get; set; }
    }

    public partial class Tile
    {
        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("imageheight")]
        public long Imageheight { get; set; }

        [JsonProperty("imagewidth")]
        public long Imagewidth { get; set; }
    }

    public partial class TileMap
    {
        public static TileMap FromJson(string json) => JsonConvert.DeserializeObject<TileMap>(json, Game.Desktop.Tiled.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this TileMap self) => JsonConvert.SerializeObject(self, Game.Desktop.Tiled.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
