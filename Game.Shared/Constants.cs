﻿using System;
using System.Collections.Generic;

namespace Game.Desktop
{
    class Constants
    {
        public class World
        {
            /// <summary>Whether or not any debug areas should be showing for physics objects</summary>
            public const Boolean DebugView = false;

            public const Boolean ConsoleOutput = false;

            public const Single GRAVITY = -500f;

            public static Single GravityMultiplier { get; set; } = 1;
        }
    }

    public class Font
    {
        public static Dictionary<Char, String> Map
        {
            get
            {
                var map = new Dictionary<Char, String>();
                String alphaBet = "abcdefghijklmnopqrstuvwxyz";
                for (Int32 i = 0; i < alphaBet.Length; i++)
                {
                    var c = alphaBet[i];
                    map.Add(c.ToString().ToUpperInvariant()[0], c.ToString().ToUpperInvariant());
                    if (i > 9) continue;
                    map.Add(i.ToString()[0], i.ToString());
                }
                map.Add('.', "dot");
                map.Add(':', "colon");
                map.Add(' ', "space");
                map.Add('%', "percentage");
                return map;
            }

        }
    }
}
