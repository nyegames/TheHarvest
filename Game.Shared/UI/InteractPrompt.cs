﻿using System;
using System.Linq;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using Game.Desktop.Glide;
using Game.Desktop.Interfaces;
using Game.Desktop.Scenes;
using Game.Desktop.Services;
using Game.Desktop.Services.Canvas;
using Game.Desktop.Services.Character;
using Game.Desktop.Services.Interactions;
using OpenTK;

namespace Game.Desktop.UI
{
    public class InteractPrompt : UIComponent
    {
        private Sprite _Sprite;
        private TextDisplay _TextDisplay;

        public override OpenTK.Vector2 Position
        {
            get => base.Position;
            set
            {
                base.Position = value;
                _Sprite.Position = value;
                _TextDisplay.Position = value;
            }
        }

        public override Boolean Visible
        {
            get => base.Visible;
            set
            {
                base.Visible = value;
                if (_Sprite != null) _Sprite.Visible = value;
                if (_TextDisplay != null) _TextDisplay.Visible = value;
            }
        }

        public InteractPrompt() : base($"{typeof(InteractPrompt).Name}")
        {
            _Sprite = new Sprite(CanvasService.UICanvas, ZService.Instance["Interact_Prompt"], Texture.GetPixel())
            {
                Size = new OpenTK.Vector2(20, 20),
                Offset = new OpenTK.Vector2(10, 10),
                Colour = new OpenTK.Vector4(0.3f, 0.6f, 0.8f, 0.9f),
                Visible = false
            };

            _TextDisplay = new TextDisplay(CanvasService.UICanvas, ZService.Instance["Interact_Text"], "Content/Packed/Fonts/KenPixel/", Font.Map)
            {
                Text = $"{0:0}",
                Scale = new OpenTK.Vector2(0.6f, 0.6f),
                Colour = new OpenTK.Vector4(1, 1, 1, 1),
                Visible = false
            };
        }

        public override void SceneLoaded(HScene scene)
        {

        }

        public override void Update()
        {
            base.Update();
            if (CharacterService.Instance.ActiveCharacter == null) return;

            var ints = InteractService.Instance.GetDetectables(CharacterService.Instance.ActiveCharacter);
            if (!ints.Any()) Hide();
            else Show(ints?.First());
        }

        private void Show(IInteractable interactable)
        {
            _TextDisplay.Text = interactable.InteractInstruction().ToUpperInvariant();
            _TextDisplay.Offset = (_TextDisplay.Size * _TextDisplay.Scale) / 2f;

            //Get the difference between the position of the object in the world and the camera it is using. (Game)
            //That will bring you to the origin position of the camera, add the origin position of the UI camera onto that (nearly always 0,0)
            Position = (interactable.CenterPosition - CanvasService.GameCanvas.Camera.Position) + CanvasService.UICanvas.Camera.Position;

            //Move the UI prompt by half the height of the character upwards
            Position += new OpenTK.Vector2(0, CharacterService.Instance.ActiveCharacter.Size.Y / 2f);

            Visible = true;
        }

        private void Hide()
        {
            Visible = false;
        }
    }
}
