﻿using System;
using AmosShared.Events;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using Game.Desktop.Characters;
using Game.Desktop.Scenes;
using Game.Desktop.Services.Canvas;
using Game.Desktop.Services.Character;
using OpenTK;

namespace Game.Desktop.UI
{
    public class HealthDisplay : UIComponent
    {
        private Character _Character;

        private readonly Sprite _Background;
        private readonly TextDisplay _TextDisplay;

        public override Vector2 Position
        {
            get => base.Position;
            set
            {
                base.Position = value;
                _Background.Position = value;

                _TextDisplay.Offset = _TextDisplay.Size / 2;
                _TextDisplay.Position = _Background.Position + new OpenTK.Vector2(_Background.Size.X / 2, -_Background.Size.Y / 2);
            }
        }

        public override Boolean Visible
        {
            get => base.Visible;
            set
            {
                base.Visible = value;
                _Background.Visible = value;
                _TextDisplay.Visible = value;
            }
        }

        public Vector2 Size => _Background.Size;

        public HealthDisplay() : base("CharacterHealthDisplay")
        {
            _Background = new NinePatchSprite(0, Texture.GetTexture("Content/Packed/Graphics/ui/white_box.png"),
                left: 6, right: 6, top: 6, bottom: 6,
                canvas: CanvasService.UICanvas)
            {
                Size = new Vector2(100, 30),
                Colour = new Vector4(89f, 155f, 255f, 60f) / 255f
            };
            _Background.Offset = new Vector2(0, _Background.Size.Y * _Background.Scale.Y);

            _TextDisplay = new TextDisplay(CanvasService.UICanvas, 1, "Content/Packed/Fonts/KenPixel/", Font.Map)
            {
                Text = $"{0:0}",
                Colour = new Vector4(1, 1, 1, 1)
            };

            EventManager.Instance.StartListen(CharacterService.CharacterLoaded, CharacterLoaded);
            EventManager.Instance.StartListen(CharacterService.CharacterLoaded, CharacterUnloaded);
            EventManager.Instance.StartListen(Character.DamageTaken, CharacterDamaged);
        }

        private void SetText(Int32 health)
        {
            _TextDisplay.Text = $"{health}";
            Position = Position;
        }

        private void CharacterLoaded(Object[] data)
        {
            _Character = CharacterService.Instance.ActiveCharacter;
            SetText(_Character.Health);
        }

        private void CharacterDied(Character obj)
        {
            SetText(0);
        }

        private void CharacterDamaged(Object[] obj)
        {
            String name = obj[0] as String;
            if (!_Character.Name.Equals(name)) return;
            SetText(_Character.Health);
        }

        private void CharacterUnloaded(Object[] data)
        {

        }

        public override void SceneLoaded(HScene scene)
        {
            Visible = scene.Name.StartsWith("Level");
            SetText(_Character?.Health ?? 0);
        }

        public void Died()
        {

        }
    }
}
