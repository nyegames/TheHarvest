﻿using System;
using System.Linq;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using Game.Desktop.Interfaces;
using Game.Desktop.Scenes;
using Game.Desktop.Services;
using Game.Desktop.Services.Attackables;
using Game.Desktop.Services.Canvas;
using Game.Desktop.Services.Character;

namespace Game.Desktop.UI
{
    public class AttackPrompt : UIComponent
    {
        private Sprite _Sprite;

        public override OpenTK.Vector2 Position
        {
            get => base.Position;
            set
            {
                base.Position = value;
                _Sprite.Position = value;
            }
        }

        public override Boolean Visible
        {
            get => base.Visible;
            set
            {
                base.Visible = value;
                if (_Sprite != null) _Sprite.Visible = value;
            }
        }

        public AttackPrompt() : base($"{typeof(AttackPrompt).Name}")
        {
            _Sprite = new Sprite(CanvasService.UICanvas, ZService.Instance["Attack_Prompt"], Texture.GetPixel())
            {
                Size = new OpenTK.Vector2(10, 10),
                Scale = new OpenTK.Vector2(1, 2),
                Colour = new OpenTK.Vector4(0.9f, 0.2f, 0.3f, 0.9f),
                Rotation = Math.PI / 4,
                Visible = false
            };
            _Sprite.Offset = _Sprite.Size / 2;
            _Sprite.RotationOrigin = _Sprite.Size / 2;
        }

        public override void SceneLoaded(HScene scene)
        {

        }

        public override void Update()
        {
            base.Update();
            if (CharacterService.Instance.ActiveCharacter == null) return;

            var ints = AttackService.Instance.GetDetectables(CharacterService.Instance.ActiveCharacter);
            if (!ints.Any()) Hide();
            else Show(ints?.First());
        }

        private void Show(IAttackable attackable)
        {
            //Get the difference between the position of the object in the world and the camera it is using. (Game)
            //That will bring you to the origin position of the camera, add the origin position of the UI camera onto that (nearly always 0,0)
            Position = (attackable.CenterPosition - CanvasService.GameCanvas.Camera.Position) + CanvasService.UICanvas.Camera.Position;

            //Move the UI prompt by half the height of the character upwards
            Position += new OpenTK.Vector2(0, CharacterService.Instance.ActiveCharacter.Size.Y / 2f);

            Visible = true;
        }

        private void Hide()
        {
            Visible = false;
        }
    }
}
