﻿using System;
using System.Collections.Generic;
using AmosShared.Interfaces;
using Game.Desktop.HObjects;
using Game.Desktop.Interfaces.Core;
using Game.Desktop.Scenes;
using OpenTK;

namespace Game.Desktop.UI
{
    // ReSharper disable once InconsistentNaming
    public abstract class UIComponent : IDisposable, IPositionable, IVisible
    {
        public virtual Vector2 Position { get; set; }

        public String Name { get; }
        private static readonly List<String> Names = new List<String>();

        public virtual Boolean Visible { get; set; } = false;

        protected UIComponent(String name)
        {
            Name = name ?? HObject.GenerateName(GetType());
            if (Names.Contains(Name)) throw new Exception($"{Name} UI component already exists");
            Names.Add(Name);
        }

        public virtual void Update()
        {

        }

        public abstract void SceneLoaded(HScene scene);

        public virtual void Dispose()
        {
            Names.Remove(Name);
        }
    }
}
