﻿using System;
using AmosShared.Events;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using Game.Desktop.Scenes;
using Game.Desktop.Services.Canvas;
using Game.Desktop.Services.Collectibles;
using OpenTK;

namespace Game.Desktop.UI
{
    public class SoulFragmentCounter : UIComponent
    {
        private readonly Sprite _Background;
        private readonly TextDisplay _TextDisplay;

        public override OpenTK.Vector2 Position
        {
            get => base.Position;
            set
            {
                base.Position = value;
                _Background.Position = value;

                _TextDisplay.Offset = _TextDisplay.Size / 2;
                _TextDisplay.Position = _Background.Position + new OpenTK.Vector2(_Background.Size.X / 2, -_Background.Size.Y / 2);
            }
        }

        public override Boolean Visible
        {
            get => base.Visible;
            set
            {
                base.Visible = value;
                _Background.Visible = value;
                _TextDisplay.Visible = value;
            }
        }

        private String _Text;
        public String Text
        {
            get => _Text;
            set
            {
                _Text = value;
                _TextDisplay.Text = value;

                //Reposition text back into the center of the background box
                Position = Position;
            }
        }

        private Single _Count;
        public Single Count
        {
            get => _Count;
            set
            {
                _Count = value;
                Text = $"{Count}";
            }
        }

        public Vector2 Size => _Background.Size;


        public SoulFragmentCounter(String name = null) : base(name)
        {
            _Background = new NinePatchSprite(0, Texture.GetTexture("Content/Packed/Graphics/ui/white_box.png"),
                left: 6, right: 6, top: 6, bottom: 6,
                canvas: CanvasService.UICanvas)
            {
                Size = new OpenTK.Vector2(40, 30),
                Colour = new OpenTK.Vector4(89f, 155f, 255f, 60f) / 255f
            };
            _Background.Offset = new OpenTK.Vector2(0, _Background.Size.Y * _Background.Scale.Y);

            _TextDisplay = new TextDisplay(CanvasService.UICanvas, 1, "Content/Packed/Fonts/KenPixel/", Font.Map)
            {
                Text = $"{0:0}",
                Colour = new OpenTK.Vector4(1, 1, 1, 1)
            };

            Count = CollectService.Instance.GetCollectablesFound().Length;

            EventManager.Instance.StartListen(CollectService.FragmentCollected, FragmentCollected);
        }

        private void FragmentCollected(Object[] data)
        {
            Count = CollectService.Instance.GetCollectablesFound().Length;
        }

        public override void SceneLoaded(HScene scene)
        {
            Visible = scene.Name.StartsWith("Level") || scene.Name.StartsWith("Lobby");
        }

        public override void Dispose()
        {
            base.Dispose();
            EventManager.Instance.StopListen(CollectService.FragmentCollected, FragmentCollected);
        }
    }
}
