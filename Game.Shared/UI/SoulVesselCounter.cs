﻿using System;
using AmosShared.Events;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using Game.Desktop.Scenes;
using Game.Desktop.Services.Canvas;
using Game.Desktop.Services.Level;
using Game.Desktop.Services.UI;
using OpenTK;

namespace Game.Desktop.UI
{
    public class SoulVesselCounter : UIComponent
    {
        private readonly Sprite _Background;
        private readonly TextDisplay _TextDisplay;

        public override OpenTK.Vector2 Position
        {
            get => base.Position;
            set
            {
                base.Position = value;
                _Background.Position = value;

                _TextDisplay.Offset = _TextDisplay.Size / 2;
                _TextDisplay.Position = _Background.Position + new OpenTK.Vector2(_Background.Size.X / 2, -_Background.Size.Y / 2);
            }
        }

        public override Boolean Visible
        {
            get => base.Visible;
            set
            {
                base.Visible = value;
                _Background.Visible = value;
                _TextDisplay.Visible = value;
            }
        }

        private String _Text;
        public String Text
        {
            get => _Text;
            set
            {
                _Text = value;
                _TextDisplay.Text = value;

                //Reposition text back into the center of the background box
                Position = Position;
            }
        }

        private Single _Count;
        public Single Count
        {
            get => _Count;
            set
            {
                _Count = value;
                Text = $"{Count:0}%";
            }
        }

        public Vector2 Size => _Background.Size;


        public SoulVesselCounter(String name = null) : base(name)
        {
            _Background = new NinePatchSprite(0, Texture.GetTexture("Content/Packed/Graphics/ui/white_box.png"),
                left: 6, right: 6, top: 6, bottom: 6,
                canvas: CanvasService.UICanvas)
            {
                Size = new OpenTK.Vector2(80, 30),
                Colour = new OpenTK.Vector4(89f, 155f, 255f, 60f) / 255f
            };
            _Background.Offset = new OpenTK.Vector2(0, _Background.Size.Y * _Background.Scale.Y);

            _TextDisplay = new TextDisplay(CanvasService.UICanvas, 1, "Content/Packed/Fonts/KenPixel/", Font.Map)
            {
                Text = $"{0:0}",
                Colour = new OpenTK.Vector4(1, 1, 1, 1)
            };

            EventManager.Instance.StartListen(LevelService.LevelProgressUpdated, VesselCollected);
            EventManager.Instance.StartListen(LevelService.OnLevelLoaded, LevelLoaded);
        }

        private void LevelLoaded(Object[] obj)
        {
            Count = 0;
        }

        private void VesselCollected(Object[] data)
        {
            Int32 count = (Int32)data[0];
            Int32 total = (Int32)data[1];
            var p = count / (Single)total;
            UiService.UITweener.TargetCancel(this);
            UiService.UITweener.Tween(this, new { Count = p * 100f }, TimeSpan.FromSeconds(1));
        }

        public override void SceneLoaded(HScene scene)
        {
            Visible = scene.Name.StartsWith("Level");
        }

        public override void Dispose()
        {
            base.Dispose();
            EventManager.Instance.StopListen(LevelService.LevelProgressUpdated, VesselCollected);
            EventManager.Instance.StopListen(LevelService.OnLevelLoaded, LevelLoaded);
        }
    }
}
