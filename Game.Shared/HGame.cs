﻿using System;
using System.IO;
using AmosShared.Base;
using AmosShared.Events;
using AmosShared.State;
using Game.Desktop.States;
using Game.Desktop.Tiled;
using Game.Shared.Input;
using OpenTK;
using OpenTK.Input;

namespace Game.Desktop
{
    public class HGame : BaseGame
    {
        public static System.Random Random = new System.Random();

        public HGame() : base("Harvest", 60) { }

        public override Vector2 InitialResolution => new Vector2(1920 * 0.5f, 1080 * 0.5f);

        public override void LoadContent()
        {
            EventManager.Instance.StartListen(KeyboardService.OnKeyPress, s => { if ((Key)s[0] == Key.Escape) this.Dispose(); });
            StateManager.Instance.StartState(new LoadingState());
        }

        protected override Vector2 CalculateExtraOffset(Single heightDifference)
        {
            return new Vector2(0, 0);
        }

        protected override void DisposeGameElements()
        {

        }
    }
}
