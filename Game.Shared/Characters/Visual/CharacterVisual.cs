﻿using System;
using System.Collections.Generic;
using System.Linq;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using AmosShared.Interfaces;
using Game.Desktop.Interfaces.Core;
using Game.Desktop.Services;
using Game.Desktop.Services.Canvas;
using OpenTK;

namespace Game.Desktop.Characters.Visual
{
    public class CharacterVisual : IUpdatable, IPositionable, IVisible, IScalable
    {
        private readonly List<String> _OutputQueue = new List<String>();

        private Vector2 _Position;
        public Vector2 Position
        {
            get => _Position;
            set
            {
                _Position = value;
                foreach (var a in _Animations.Values) a.Position = value + a.Offset / 2;
            }
        }

        private Boolean _Visible;
        public Boolean Visible
        {
            get => _Visible;
            set
            {
                _Visible = value;
                var k = _AnimationQueue.Peek();
                foreach (var a in _Animations.Values) a.Visible = value && a.Equals(_Animations[k]);
            }
        }

        private Vector2 _Scale;
        public Vector2 Scale
        {
            get => _Scale;
            set
            {
                _Scale = value;
                foreach (var a in _Animations.Values) a.Scale = _Scale;
            }
        }

        public Boolean IsDisposed { get; set; }

        public Sprite CurrentSprite => _Animations[_AnimationQueue.Peek()];

        private readonly Dictionary<String, AnimatedSprite> _Animations = new Dictionary<String, AnimatedSprite>();

        private readonly Queue<String> _AnimationQueue = new Queue<String>();

        public Boolean LoopLastAnimation { get; set; }

        private readonly Dictionary<String, List<Action>> _OnCompleteActions = new Dictionary<String, List<Action>>();

        public CharacterVisual()
        {
            LoadAnimations();
            ConfigureAnimations();

            PlayAnimation("Idle", true);
            Visible = true;
        }

        private void LoadAnimations()
        {
            var canvas = CanvasService.GameCanvas;
            var z = ZService.Instance["Character"];

            var fps = 10;

            _Animations.Add("Idle", new AnimatedSprite(canvas, z, new Texture[]
            {
                Texture.GetTexture("Content/Packed/Graphics/bluebody/Bluebody_Idle_0.png")
            }, fps));

            _Animations.Add("Walk", new AnimatedSprite(canvas, z, new Texture[]
            {
                Texture.GetTexture("Content/Packed/Graphics/bluebody/Bluebody_Walk_0.png"),
                Texture.GetTexture("Content/Packed/Graphics/bluebody/Bluebody_Walk_1.png"),
            }, 18));

            _Animations.Add("Jump", new AnimatedSprite(canvas, z, new Texture[]
            {
                Texture.GetTexture("Content/Packed/Graphics/bluebody/Bluebody_Jump_0.png"),
            }, fps));

            _Animations.Add("Crouch", new AnimatedSprite(canvas, z, new Texture[]
            {
                Texture.GetTexture("Content/Packed/Graphics/bluebody/Bluebody_Crouch_0.png"),
            }, fps));

            _Animations.Add("Interact", new AnimatedSprite(canvas, z, new Texture[]
            {
                Texture.GetTexture("Content/Packed/Graphics/bluebody/Bluebody_Interact_0.png"),
            }, 12));

            _Animations.Add("Attack", new AnimatedSprite(canvas, z, new Texture[]
            {
                Texture.GetTexture("Content/Packed/Graphics/bluebody/Bluebody_Attack_0.png"),
            }, 12));

            _Animations.Add("WallSlide", new AnimatedSprite(canvas, z, new Texture[]
            {
                Texture.GetTexture("Content/Packed/Graphics/bluebody/Bluebody_Climb_0.png"),
            }, fps));
        }

        private void ConfigureAnimations()
        {
            foreach (var anim in _Animations.Values)
            {
                anim.Offset = new Vector2(anim.Size.X / 2, 0f);
                anim.ScaleOrigin = new Vector2(anim.Size.X / 2, 0f);
                anim.AnimEndBehaviour = AnimatedSprite.EndBehaviour.STOP;
                anim.OnComplete += AnimationComplete;
            }

            foreach (var s in _Animations.Keys)
            {
                _OnCompleteActions.Add(s, new List<Action>());
            }

            PlayAnimation("Idle");
        }

        internal void FaceDirection(Int32 direction)
        {
            direction = direction >= 0 ? 1 : -1;
            Scale = new Vector2(direction, 1f);
        }

        public void AddEndAction(String key, Action action)
        {
            _OnCompleteActions[key].Add(action);
        }

        public void PlayAnimation(String key, Boolean force = true) => PlayAnimation(new[] { key }, force);

        public void PlayAnimation(String[] keys, Boolean force = true)
        {
            if (force)
            {
                _AnimationQueue.Clear();
                foreach (var s in keys) _AnimationQueue.Enqueue(s);
                StartAnimation(_AnimationQueue.Peek());
            }
            else
            {
                foreach (var k in keys)
                {
                    _AnimationQueue.Enqueue(k);
                }
            }
        }

        private void StartAnimation(String key)
        {
            foreach (var anims in _Animations)
            {
                anims.Value.Visible = false;
                anims.Value.Playing = false;
            }

            _Animations[key].CurrentFrame = 0;
            _Animations[key].Visible = Visible;
            _Animations[key].Playing = true;

            if (_OutputQueue.Count > 0 && _OutputQueue.Last().Equals(key)) return;
            _OutputQueue.Add(key);
            if (_OutputQueue.Count >= 5) _OutputQueue.RemoveAt(0);
        }

        private void AnimationComplete(AnimatedSprite obj)
        {
            String lastKey = _AnimationQueue.Dequeue();

            foreach (var a in _OnCompleteActions[lastKey].ToList()) a?.Invoke();
            _OnCompleteActions[lastKey].Clear();

            if (_AnimationQueue.Count == 0) _AnimationQueue.Enqueue(LoopLastAnimation ? lastKey : "Idle");

            StartAnimation(_AnimationQueue.Peek());
            PlayAnimation(_AnimationQueue.Peek());
        }

        public Boolean CanUpdate() => !IsDisposed;

        public void Update()
        {

        }

        internal void ApplyForce(Vector2 direction, Single power)
        {

        }

        public void Output()
        {
            Console.WriteLine("Animations Played:");
            for (Int32 i = 0; i < _OutputQueue.Count; i++)
            {
                Console.WriteLine($"  {i + 1}, {_OutputQueue[i]}");
            }
        }

        public void Dispose()
        {
            foreach (var a in _Animations.Values)
            {
                a.OnComplete -= AnimationComplete;
                a.Dispose();
            }
            _Animations.Clear();
            _AnimationQueue.Clear();
        }
    }
}
