﻿namespace Game.Desktop.Characters.Controller.Commands
{
    public class JumpCommand : CharacterCommand
    {
        protected override void OnExecute(Character character)
        {
            character.StartJump(0);
        }
    }
}
