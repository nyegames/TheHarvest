﻿namespace Game.Desktop.Characters.Controller.Commands
{
    public class CrouchCommand : CharacterCommand
    {
        protected override void OnExecute(Character character)
        {
            character.Crouch();
        }
    }
}
