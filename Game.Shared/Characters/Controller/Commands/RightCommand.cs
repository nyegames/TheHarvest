﻿namespace Game.Desktop.Characters.Controller.Commands
{
    class RightCommand : CharacterCommand
    {
        protected override void OnExecute(Character character)
        {
            character.Move(1);
        }
    }
}
