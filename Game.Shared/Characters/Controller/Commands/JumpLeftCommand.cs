﻿namespace Game.Desktop.Characters.Controller.Commands
{
    public class JumpLeftCommand : CharacterCommand
    {
        protected override void OnExecute(Character character)
        {
            character.StartJump(-1);
        }
    }
}
