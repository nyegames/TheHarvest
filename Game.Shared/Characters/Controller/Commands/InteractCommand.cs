﻿namespace Game.Desktop.Characters.Controller.Commands
{
    public class InteractCommand : CharacterCommand
    {
        protected override void OnExecute(Character character)
        {
            character.Interact();
        }
    }
}
