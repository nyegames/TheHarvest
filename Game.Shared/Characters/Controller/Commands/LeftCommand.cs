﻿namespace Game.Desktop.Characters.Controller.Commands
{
    public class LeftCommand : CharacterCommand
    {
        protected override void OnExecute(Character character)
        {
            character.Move(-1);
        }
    }
}
