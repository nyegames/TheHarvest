﻿namespace Game.Desktop.Characters.Controller.Commands
{
    public class AttackCommand : CharacterCommand
    {
        protected override void OnExecute(Character character)
        {
            character.Attack();
        }
    }
}
