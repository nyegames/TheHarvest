﻿namespace Game.Desktop.Characters.Controller.Commands
{
    public abstract class CharacterCommand
    {
        protected Character _Character;

        public CharacterCommand()
        {

        }

        /// <summary>Perform the command using the given Character as reference</summary>
        /// <param name="character"></param>
        public void Execute(Character character)
        {
            _Character = character;
            OnExecute(_Character);
        }

        /// <summary>Override this to perform the behaviour for this specific command</summary>
        /// <param name="character"></param>
        protected abstract void OnExecute(Character character);
    }
}
