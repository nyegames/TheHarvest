﻿namespace Game.Desktop.Characters.Controller.Commands
{
    public class JumpRightCommand : CharacterCommand
    {
        protected override void OnExecute(Character character)
        {
            character.StartJump(1);
        }
    }
}
