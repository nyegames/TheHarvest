﻿using System;
using AmosShared.Interfaces;
using Game.Shared.Input;

namespace Game.Desktop.Characters.Controller.States
{
    public abstract class CharacterState : IUpdatable
    {
        public Boolean IsDisposed { get; set; }

        public Character Character { get; private set; }

        private readonly CharacterController _Controller;

        protected String Left = "Left";
        protected String Right = "Right";
        protected String Interact = "Interact";
        protected String Attack = "Attack";
        protected String Crouch = "Crouch";
        protected String Jump = "Jump";

        protected CharacterState(CharacterController controller)
        {
            _Controller = controller;
        }

        protected void ChangeState(String name)
        {
            _Controller.ChangeState(name);
        }

        public virtual void Enter(Character character) => Character = character;

        public Boolean CanUpdate() => !IsDisposed;

        public virtual void Update()
        {

        }

        protected InputBehaviour.State GetBehaviourState(String name)
        {
            return InputService.Instance.GetBehaviour(name)?.BehaviourState ?? InputBehaviour.State.Off;
        }

        protected TimeSpan GetBehaviourHeldTime(String name)
        {
            return GetBehaviour(name)?.HeldTime ?? TimeSpan.Zero;
        }

        protected InputBehaviour GetBehaviour(String name)
        {
            return InputService.Instance.GetBehaviour(name);
        }

        protected Boolean BehaviourActive(String name) => GetBehaviourState(name) != InputBehaviour.State.Off;

        public virtual void Exit()
        {

        }

        public void Dispose()
        {
            Character = null;
        }
    }
}