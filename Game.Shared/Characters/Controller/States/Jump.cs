﻿using System;
using Game.Desktop.Characters.Controller.Commands;
using Game.Shared.Input;

namespace Game.Desktop.Characters.Controller.States
{
    internal class Jump : CharacterState
    {
        private TimeSpan _JumpTimer = TimeSpan.FromSeconds(0.17);
        private Boolean _IgnoreMove = false;

        public Jump(CharacterController controller) : base(controller)
        {
        }

        public override void Enter(Character character)
        {
            base.Enter(character);

            _IgnoreMove = true;
            if (Character.TouchingLeft && !character.Grounded)
            {
                new JumpRightCommand().Execute(Character);
            }
            else if (Character.TouchingRight && !character.Grounded)
            {
                new JumpLeftCommand().Execute(Character);
            }
            else
            {
                new JumpCommand().Execute(Character);
                _IgnoreMove = false;
            }

            character.Visual.LoopLastAnimation = true;
            character.Visual.PlayAnimation("Jump");
        }

        public override void Update()
        {
            base.Update();

            if (!BehaviourActive(Jump) || Character.TouchingUp || GetBehaviourHeldTime(Jump) >= TimeSpan.FromSeconds(0.17))
            {
                ChangeState("Fall");
            }
            else if (GetBehaviourState(Attack) == InputBehaviour.State.Pressed)
            {
                ChangeState("AerialAttack");
            }
            else if (GetBehaviourState(Interact) == InputBehaviour.State.Pressed)
            {
                ChangeState("AerialInteract");
            }
            else if (!_IgnoreMove && (BehaviourActive(Left) || BehaviourActive(Right))
            )
            {
                if (BehaviourActive(Left)) new LeftCommand().Execute(Character);
                else new RightCommand().Execute(Character);
            }
        }

    }
}
