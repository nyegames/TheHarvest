﻿using System;
using Game.Shared.Input;

namespace Game.Desktop.Characters.Controller.States
{
    public class Idle : CharacterState
    {
        public Idle(CharacterController controller) : base(controller)
        {
        }

        public override void Enter(Character character)
        {
            base.Enter(character);

            if (Character.CanJump && GetBehaviourState(Jump) == InputBehaviour.State.Pressed)
            {
                ChangeState("Jump");
            }
            else if (BehaviourActive(Left) || BehaviourActive(Right))
            {
                ChangeState("Walk");
            }
            else if (Character.Grounded && Math.Abs(Character.Velocity.X) > 0.01f)
            {
                ChangeState("GroundSlide");
            }
            else
            {
                character.Idle();
                character.Visual.PlayAnimation("Idle");
            }
        }

        public override void Update()
        {
            base.Update();

            if (Character.CanJump && GetBehaviourState(Jump) == InputBehaviour.State.Pressed)
            {
                ChangeState("Jump");
            }
            else if (!Character.Grounded)
            {
                ChangeState("Fall");
            }
            else if (Character.Grounded && Math.Abs(Character.Velocity.X) > 0.01f)
            {
                ChangeState("GroundSlide");
            }
            else if (BehaviourActive(Crouch))
            {
                ChangeState("Crouch");
            }
            else if (BehaviourActive(Left) || BehaviourActive(Right))
            {
                ChangeState("Walk");
            }
            else if (GetBehaviourState(Interact) == InputBehaviour.State.Pressed)
            {
                ChangeState("Interact");
            }
            else if (GetBehaviourState(Attack) == InputBehaviour.State.Pressed)
            {
                ChangeState("Attack");
            }
        }
    }
}
