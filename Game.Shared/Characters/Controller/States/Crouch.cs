﻿using System;
using Game.Desktop.Characters.Controller.Commands;
using Game.Shared.Input;

namespace Game.Desktop.Characters.Controller.States
{
    public class Crouch : CharacterState
    {
        public Crouch(CharacterController controller) : base(controller)
        {
        }

        public override void Enter(Character character)
        {
            base.Enter(character);
            new CrouchCommand().Execute(character);
            character.Visual.LoopLastAnimation = true;
            character.Visual.PlayAnimation("Crouch");

            if (Character.CanJump && GetBehaviourState(Jump) == InputBehaviour.State.Pressed)
            {
                ChangeState("Jump");
            }
        }

        public override void Update()
        {
            base.Update();

            if (Character.CanJump && GetBehaviourState(Jump) == InputBehaviour.State.Pressed)
            {
                ChangeState("Jump");
            }
            else if (!Character.Grounded)
            {
                ChangeState("Fall");
            }
            else if (GetBehaviourState(Interact) == InputBehaviour.State.Pressed)
            {
                ChangeState("CrouchInteract");
            }
            else if (GetBehaviourState(Attack) == InputBehaviour.State.Pressed)
            {
                ChangeState("CrouchAttack");
            }


            if (GetBehaviourState(Crouch) == InputBehaviour.State.Held) return;
            if (Math.Abs(Character.Velocity.X) >= 0.01f)
            {
                ChangeState("GroundSlide");
            }
            else
            {
                ChangeState("Idle");
            }
        }
    }
}
