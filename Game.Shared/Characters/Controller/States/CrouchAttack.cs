﻿using Game.Desktop.Characters;
using Game.Desktop.Characters.Controller;
using Game.Desktop.Characters.Controller.Commands;
using Game.Desktop.Characters.Controller.States;
using System;
using Game.Shared.Input;

namespace Game.Shared.Characters.Controller.States
{
    public class CrouchAttack : CharacterState
    {
        public CrouchAttack(CharacterController controller) : base(controller)
        {

        }

        public override void Enter(Character character)
        {
            base.Enter(character);

            new AttackCommand().Execute(character);

            Character.Attack();

            Character.Visual.LoopLastAnimation = false;
            Character.Visual.AddEndAction("Attack", () => Character.Attacking = false);
            Character.Visual.PlayAnimation("Attack", true);
        }

        public override void Update()
        {
            base.Update();
            if (Character.Attacking) return;
            if (GetBehaviourState(Crouch) == InputBehaviour.State.Held)
            {
                ChangeState("Crouch");
            }
            else if (Math.Abs(Character.Velocity.X) >= 0.01f)
            {
                ChangeState("GroundSlide");
            }
            else
            {
                ChangeState("Idle");
            }
        }
    }
}
