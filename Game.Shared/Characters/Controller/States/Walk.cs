﻿using System;
using Game.Desktop.Characters.Controller.Commands;
using Game.Shared.Input;

namespace Game.Desktop.Characters.Controller.States
{
    public class Walk : CharacterState
    {
        public Walk(CharacterController controller) : base(controller)
        {
        }

        public override void Enter(Character character)
        {
            base.Enter(character);
            if (GetBehaviourState(Left) != InputBehaviour.State.Off) new LeftCommand().Execute(Character);
            else new RightCommand().Execute(Character);

            character.Visual.LoopLastAnimation = true;
            character.Visual.PlayAnimation("Walk");

            if (Character.CanJump && GetBehaviourState(Jump) == InputBehaviour.State.Pressed)
            {
                ChangeState("Jump");
            }
        }

        public override void Update()
        {
            base.Update();

            if (Character.CanJump && GetBehaviourState(Jump) == InputBehaviour.State.Pressed)
            {
                ChangeState("Jump");
            }
            else if (!Character.Grounded)
            {
                ChangeState("Fall");
            }
            else if (BehaviourActive(Left) || BehaviourActive(Right))
            {
                if (BehaviourActive(Left)) new LeftCommand().Execute(Character);
                else new RightCommand().Execute(Character);

                if (BehaviourActive(Crouch))
                {
                    Character.StopMove();
                    ChangeState("Crouch");
                }
                else if (GetBehaviourState(Interact) == InputBehaviour.State.Pressed)
                {
                    ChangeState("Interact");
                }
                else if (GetBehaviourState(Attack) == InputBehaviour.State.Pressed)
                {
                    ChangeState("Attack");
                }
            }
            else
            {
                if (Math.Abs(Character.Velocity.X) >= 0.01f)
                {
                    ChangeState("GroundSlide");
                }
                else
                {
                    ChangeState("Idle");
                }
            }
        }
    }
}
