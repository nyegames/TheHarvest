﻿using System;
using Game.Desktop.Characters;
using Game.Desktop.Characters.Controller;
using Game.Desktop.Characters.Controller.States;
using Game.Shared.Input;

namespace Game.Shared.Characters.Controller.States
{
    public class GroundSliding : CharacterState
    {
        public GroundSliding(CharacterController controller) : base(controller)
        {

        }

        public override void Enter(Character character)
        {
            base.Enter(character);
            Character.LandJump();

            if (Character.CanJump && GetBehaviourState(Jump) == InputBehaviour.State.Pressed)
            {
                ChangeState("Jump");
            }
            else if (BehaviourActive(Left) || BehaviourActive(Right))
            {
                ChangeState("Walk");
            }
            else
            {
                character.Idle();
                character.Visual.PlayAnimation("Idle");
            }
        }

        public override void Update()
        {
            base.Update();

            if (Character.CanJump && GetBehaviourState(Jump) == InputBehaviour.State.Pressed)
            {
                ChangeState("Jump");
            }
            else if (!Character.Grounded)
            {
                ChangeState("Fall");
            }
            else if (BehaviourActive(Crouch))
            {
                ChangeState("Crouch");
            }
            else if (BehaviourActive(Left) || BehaviourActive(Right))
            {
                ChangeState("Walk");
            }
            else if (GetBehaviourState(Interact) == InputBehaviour.State.Pressed)
            {
                ChangeState("Interact");
            }
            else if (GetBehaviourState(Attack) == InputBehaviour.State.Pressed)
            {
                ChangeState("Attack");
            }
            else if (Math.Abs(Character.Velocity.X) < 0.01f)
            {
                ChangeState("Idle");
            }
        }
    }
}
