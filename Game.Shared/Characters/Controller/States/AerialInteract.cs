﻿using System;
using Game.Desktop.Characters.Controller.Commands;

namespace Game.Desktop.Characters.Controller.States
{
    public class AerialInteract : CharacterState
    {
        public AerialInteract(CharacterController controller) : base(controller)
        {

        }

        public override void Enter(Character character)
        {
            base.Enter(character);
            new InteractCommand().Execute(character);

            Character.Interact();

            Character.Visual.LoopLastAnimation = false;
            Character.Visual.AddEndAction("Interact", () => Character.Interacting = false);
            Character.Visual.PlayAnimation("Interact", true);
        }

        public override void Update()
        {
            base.Update();
            if (Character.Interacting) return;
            ChangeState("Fall");
        }
    }
}
