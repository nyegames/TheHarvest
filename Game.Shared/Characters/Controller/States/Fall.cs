﻿using System;
using Game.Desktop.Characters.Controller.Commands;
using Game.Shared.Input;

namespace Game.Desktop.Characters.Controller.States
{
    class Fall : CharacterState
    {
        public Fall(CharacterController controller) : base(controller)
        {
        }

        public override void Enter(Character character)
        {
            base.Enter(character);
            Character.StartFall();

            character.Visual.LoopLastAnimation = true;
            character.Visual.PlayAnimation("Jump");

            if (Character.CanJump && GetBehaviourState(Jump) == InputBehaviour.State.Pressed)
            {
                ChangeState("Jump");
            }
        }

        public override void Update()
        {
            base.Update();

            if (Character.CanJump && GetBehaviourState(Jump) == InputBehaviour.State.Pressed)
            {
                ChangeState("Jump");
            }
            else if (!Character.Grounded && (Character.TouchingLeft || Character.TouchingRight))
            {
                ChangeState("WallSlide");
            }
            else if (Character.Grounded)
            {
                if (BehaviourActive(Crouch))
                {
                    ChangeState("Crouch");
                }
                else if (Character.Velocity.X > 0)
                {
                    ChangeState("GroundSlide");
                }
                else ChangeState("Idle");
            }
            else if (GetBehaviourState(Attack) == InputBehaviour.State.Pressed)
            {
                ChangeState("AerialAttack");
            }
            else if (GetBehaviourState(Interact) == InputBehaviour.State.Pressed)
            {
                ChangeState("AerialInteract");
            }
            if (BehaviourActive(Left) || BehaviourActive(Right))
            {
                if (BehaviourActive(Left)) new LeftCommand().Execute(Character);
                else new RightCommand().Execute(Character);
            }
            else Character.StopMove();
        }
    }
}
