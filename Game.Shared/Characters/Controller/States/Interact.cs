﻿using System;
using Game.Desktop.Characters.Controller.Commands;
using Game.Shared.Input;

namespace Game.Desktop.Characters.Controller.States
{
    public class Interact : CharacterState
    {
        public Interact(CharacterController controller) : base(controller)
        {
        }

        public override void Enter(Character character)
        {
            base.Enter(character);
            new InteractCommand().Execute(character);

            Character.Interact();

            Character.Visual.LoopLastAnimation = false;
            Character.Visual.AddEndAction("Interact", () => Character.Interacting = false);
            Character.Visual.PlayAnimation("Interact", true);
        }

        public override void Update()
        {
            base.Update();
            if (Character.Interacting) return;
            if (GetBehaviourState(Crouch) == InputBehaviour.State.Held)
            {
                ChangeState("Crouch");
            }
            else if (Math.Abs(Character.Velocity.X) >= 0.01f)
            {
                ChangeState("GroundSlide");
            }
            else
            {
                ChangeState("Idle");
            }
        }
    }
}
