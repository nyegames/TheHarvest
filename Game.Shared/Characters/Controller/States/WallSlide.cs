﻿using System;
using Game.Desktop.Characters.Controller.Commands;
using Game.Shared.Input;

namespace Game.Desktop.Characters.Controller.States
{
    public class WallSlide : CharacterState
    {
        public WallSlide(CharacterController controller) : base(controller)
        {
        }

        public override void Enter(Character character)
        {
            base.Enter(character);
            Character.AddJumpCharge(1);

            character.Visual.LoopLastAnimation = true;
            character.Visual.PlayAnimation("WallSlide");

            character.StopMove();

            if (Character.CanJump && GetBehaviourState(Jump) == InputBehaviour.State.Pressed)
            {
                ChangeState("Jump");
            }
        }

        public override void Update()
        {
            base.Update();

            if (Character.Grounded)
            {
                if (Math.Abs(Character.Velocity.X) > 0.01f)
                {
                    ChangeState("GroundSlide");
                }
                else
                {
                    ChangeState("Idle");
                }
            }
            else if (Character.TouchingRight && BehaviourActive(Left) || Character.TouchingLeft && BehaviourActive(Right))
            {
                if (BehaviourActive(Left)) new LeftCommand().Execute(Character);
                else new RightCommand().Execute(Character);
                ChangeState("Fall");
            }
            else if (Character.CanJump && GetBehaviourState(Jump) == InputBehaviour.State.Pressed)
            {
                ChangeState("Jump");
            }
            else if (!Character.TouchingLeft && !Character.TouchingRight)
            {
                ChangeState("Fall");
            }
        }
    }
}
