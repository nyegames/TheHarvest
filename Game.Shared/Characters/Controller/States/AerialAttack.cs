﻿using System;
using Game.Desktop.Characters.Controller.Commands;

namespace Game.Desktop.Characters.Controller.States
{
    public class AerialAttack : CharacterState
    {
        public AerialAttack(CharacterController controller) : base(controller)
        {

        }

        public override void Enter(Character character)
        {
            base.Enter(character);

            new AttackCommand().Execute(character);

            Character.Attack();

            Character.Visual.LoopLastAnimation = false;
            Character.Visual.AddEndAction("Attack", () => Character.Attacking = false);
            Character.Visual.PlayAnimation("Attack", true);
        }

        public override void Update()
        {
            base.Update();
            if (Character.Attacking) return;
            ChangeState("Fall");
        }
    }
}
