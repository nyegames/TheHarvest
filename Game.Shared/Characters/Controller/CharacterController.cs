﻿using System;
using System.Collections.Generic;
using AmosShared.Interfaces;
using Game.Desktop.Characters.Controller.States;
using Game.Shared.Characters.Controller.States;

namespace Game.Desktop.Characters.Controller
{
    public sealed class CharacterController : IUpdatable
    {
        private readonly List<String> _PreviousStates = new List<String>();

        private readonly Character _Character;

        private readonly Dictionary<String, CharacterState> _RegisteredStates;

        private CharacterState _CurrentState;
        private CharacterState _NextState;

        public CharacterController(Character character)
        {
            _RegisteredStates = new Dictionary<String, CharacterState>()
            {
                {"Idle", new Idle(this) },
                {"GroundSlide", new GroundSliding(this) },
                {"Walk", new Walk(this) },

                {"Crouch", new Crouch(this) },
                {"CrouchAttack", new CrouchAttack(this) },
                {"CrouchInteract", new CrouchInteract(this) },

                {"Attack", new Attack(this) },
                {"Interact", new Interact(this) },

                {"Jump", new Jump(this) },
                {"Fall", new Fall(this) },

                {"AerialAttack", new AerialAttack(this) },
                {"AerialInteract", new AerialAttack(this) },

                {"WallSlide", new WallSlide(this) }
            };

            _Character = character;
            _CurrentState = _RegisteredStates["Idle"];
            _CurrentState.Enter(_Character);
        }

        private String GetStateName(CharacterState state)
        {
            var currentName = "";
            foreach (var kv in _RegisteredStates)
            {
                if (!kv.Value.Equals(state)) continue;
                currentName = kv.Key;
                break;
            }
            return currentName;
        }

        internal void ChangeState(String name)
        {
            _PreviousStates.Add(GetStateName(_CurrentState));
            if (_PreviousStates.Count > 5) _PreviousStates.RemoveAt(0);

            _NextState = _RegisteredStates[name];
        }

        public Boolean CanUpdate() => !IsDisposed;
        public void Update()
        {
            if (_NextState != null)
            {
                _CurrentState.Exit();

                _CurrentState = _NextState;
                _NextState = null;

                _CurrentState.Enter(_Character);
            }
            else
            {
                _CurrentState.Update();
            }
        }

        public void Output()
        {
            Console.WriteLine("Controller Information:");
            for (Int32 i = 0; i < _PreviousStates.Count; i++)
            {
                Console.WriteLine($"  {i} - {_PreviousStates[i]}");
            }
            Console.WriteLine($"Current - {GetStateName(_CurrentState)}");
        }

        public Boolean IsDisposed { get; set; }
        public void Dispose()
        {
            foreach (var s in _RegisteredStates.Values) s.Dispose();
            _RegisteredStates.Clear();
        }
    }
}
