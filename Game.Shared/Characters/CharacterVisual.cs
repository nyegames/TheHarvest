﻿//using AmosShared.Graphics;
//using AmosShared.Graphics.Drawables;
//using AmosShared.Interfaces;
//using Game.Shared.Characters;
//using Game.Shared.Services;
//using OpenTK;
//using System;

//namespace Game.Shared.CharacterVisuals
//{
//    public class CharacterVisual : IUpdatable, IPositionable
//    {
//        private Vector2 _Position;
//        public Vector2 Position
//        {
//            get => _Position;
//            set => _Position = value;
//        }

//        public bool IsDisposed { get; set; }

//        public AnimatedSprite AnimatedSprite { get; set; }

//        private Character _Character;

//        protected Dictionary<String, List<Action<AnimatedSprite>>> _StartAction = new Dictionary<string, List<Action<AnimatedSprite>>>();

//        public CharacterVisual(Character character)
//        {
//            _Character = character;
//            var RenderCanvas = CanvasManager.GameCanvas;
//            var ActorZ = ZService.Instance["Character"];

//            LoadAnimations();
//            Animate("Idle");
//        }

//        /// <summary>Load all of the animations used by this <see cref="Actor"/></summary>
//        /// <param name="animations"></param>
//        protected virtual void LoadAnimations()
//        {
//            SpritesheetLoader.LoadSheet("Content/Packed/Graphics/bluebody/", "bluebody.png", "bluebody.json");

//            OpenTK.Vector2 scaleFactor = new OpenTK.Vector2(0.7f, 0.9f);

//            AddPresence("Idle", new[] {
//                Texture.GetTexture("Content/Packed/Graphics/bluebody/idle/Bluebody_Idle_0.png")
//            }, 20f, 60f);

//            AddPresence("Walk", new[] {
//                Texture.GetTexture("Content/Packed/Graphics/bluebody/walk/Bluebody_Walk_0.png"),
//                Texture.GetTexture("Content/Packed/Graphics/bluebody/walk/Bluebody_Walk_1.png"),
//            }, 20f, 60f);

//            AddPresence("Jump", new[] {
//                Texture.GetTexture("Content/Packed/Graphics/bluebody/jump/Bluebody_Jump_0.png"),
//            }, 20f, 60f);

//            AddPresence("Crouch", new[] {
//                Texture.GetTexture("Content/Packed/Graphics/bluebody/crouch/Bluebody_Crouch_0.png"),
//            }, 20f, 40f);

//            AddPresence("Interact", new[] {
//                Texture.GetTexture("Content/Packed/Graphics/bluebody/interact/Bluebody_Interact_0.png"),
//            }, 20f, 60f);

//            AddPresence("Attack", new[] {
//                Texture.GetTexture("Content/Packed/Graphics/bluebody/attack/Bluebody_Attack_0.png"),
//            }, 20f, 60f);
//        }

//        /// <summary>Play the given <paramref name="animKey"/> and choose whether it starts immedaitely or when its ready</summary>
//        /// <param name="animKey"></param>
//        /// <param name="force"></param>
//        public void Animate(String animKey, Boolean force = false) => Animate(new string[] { animKey }, force);

//        /// <summary>Play a sequence of animations, choose whether they are played immedately or when they are ready</summary>
//        /// <param name="sequence"></param>
//        /// <param name="force"></param>
//        public void Animate(String[] sequence, Boolean force = false)
//        {
//            if (force)
//            {
//                foreach (var a in _AnimationSequence.ToList())
//                {
//                    _ActorPresences[a].Visible = false;
//                    _ActorPresences[a].Playing = false;
//                }
//                _AnimationSequence.Clear();
//            }
//            foreach (var s in sequence) _AnimationSequence.Enqueue(s);

//            RestartCurrent();

//            MatchActors();
//        }

//        /// <summary>Removes all linked <see cref="OnAnimationEnd(AnimatedSprite)"/> actions
//        /// and restarts the <see cref="CurrentPresence.Sprite"/>
//        /// invokes all <see cref="_StartAction"/></summary>
//        private void RestartCurrent()
//        {
//            foreach (var p in _ActorPresences.Values) p.Sprite.OnComplete -= OnAnimationEnd;

//            var current = CurrentPresence.Sprite;
//            current.Visible = true;
//            current.CurrentFrame = 0;
//            current.Playing = true;

//            current.OnComplete += OnAnimationEnd;

//            foreach (var start in _StartAction[_AnimationSequence.Peek()]) start.Invoke(current);
//        }

//        /// <summary>Invoked when the current animation has ended</summary>
//        /// <param name="anim"></param>
//        protected virtual void OnAnimationEnd(AnimatedSprite anim)
//        {
//            anim.Visible = false;
//            anim.Playing = false;

//            if (_AnimationSequence.Count > 1) _AnimationSequence.Dequeue();

//            RestartCurrent();

//            MatchActors();
//        }

//        public void AddStartAction(String key, Action<AnimatedSprite> action)
//        {
//            _StartAction[key].Add(action);
//        }
//        public void RemoveStartAction(String key, Action<AnimatedSprite> action)
//        {
//            _StartAction[key].Remove(action);
//        }

//        public void AddFrameAction(String key, Action<AnimatedSprite> action, Int32 frame, Int32 repeats = 0)
//        {
//            _ActorPresences[key].Sprite.AddFrameAction(action, frame, repeats);
//        }
//        public void RemoveFrameAction(String key, Action<AnimatedSprite> action, Int32 frame)
//        {
//            _ActorPresences[key].Sprite.RemoveFrameAction(action, frame);
//        }

//        public void AddEndAction(String key, Action<AnimatedSprite> action)
//        {
//            _ActorPresences[key].Sprite.OnComplete += action;
//        }
//        public void RemoveEndAction(String key, Action<AnimatedSprite> action)
//        {
//            _ActorPresences[key].Sprite.OnComplete -= action;
//        }


//        public bool CanUpdate() => !IsDisposed;

//        public void Update(TimeSpan timeTilUpdate)
//        {

//        }

//        public void Dispose()
//        {

//        }
//    }
//}
