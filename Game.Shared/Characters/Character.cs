﻿using System;
using System.Dynamic;
using System.Linq;
using AmosShared.Base;
using AmosShared.Events;
using AmosShared.Graphics;
using AmosShared.Graphics.Drawables;
using AmosShared.Interfaces;
using Game.Desktop.Characters.Controller;
using Game.Desktop.Characters.Visual;
using Game.Desktop.Health;
using Game.Desktop.HObjects;
using Game.Desktop.HObjects.TileMap;
using Game.Desktop.HObjects.TileMap.Tile;
using Game.Desktop.Interfaces;
using Game.Desktop.Services;
using Game.Desktop.Services.Attackables;
using Game.Desktop.Services.Canvas;
using Game.Desktop.Services.Interactions;
using Game.Desktop.Services.Level;
using Game.Desktop.Services.Scenes;
using Game.Desktop.Services.SnapShot;
using OpenTK;
using OpenTK.Input;

namespace Game.Desktop.Characters
{
    public sealed class Character : HObject,
        ISizable, IPositionable,
        IInteractor, IAttacker,
        ISnapShottable,
        IAttackable
    {
        public const String DamageTaken = "Character-DamageTaken";

        private Vector2 _Position;

        #region Debug

        private Sprite DebugSprite { get; set; }

        private readonly Sprite _DebugPositionDot;

        private readonly Sprite _PetPoly;

        #endregion

        #region TileMap

        private HTileMap _TileMap;

        private Int32[] _GridCoords = new[] { 0, 0 };

        /// <summary>Is the Character touching a tile below it</summary>
        public Boolean Grounded { get; private set; } = true;

        /// <summary>The tile the player is touching below it</summary>
        public HTile GroundTile { get; internal set; }

        /// <summary>Is ther Character touching a tile to the left</summary>
        public Boolean TouchingLeft { get; private set; } = false;

        /// <summary>The tile the player is touching to the left of it</summary>
        public HTile LeftTile { get; internal set; }

        /// <summary>Is the Character touching a tile to the right</summary>
        public Boolean TouchingRight { get; private set; } = false;

        /// <summary>The tile the player is touching to the right</summary>
        public HTile RightTile { get; internal set; }

        /// <summary>Is the Character touching a tile above it</summary>
        public Boolean TouchingUp { get; private set; } = false;

        /// <summary>The tile the player is touching above it</summary>
        public HTile UpTile { get; internal set; }

        #endregion

        #region CharacterComponents

        public readonly CharacterVisual Visual;

        private readonly CharacterController _Controller;

        private readonly HealthComponent _Health;

        #endregion

        #region UserActions

        /// <summary>True while an attack is occuring</summary>
        public Boolean Attacking { get; internal set; }
        /// <summary>True while interacting is occuring</summary>
        public Boolean Interacting { get; internal set; }
        /// <summary>True while the user initiates and is currently crouching</summary>
        public Boolean Crouching { get; internal set; }

        #endregion

        #region Jumping

        public Single JumpPower { get; private set; } = 700f;

        public Single CurrentJumpPower { get; private set; } = 0f;

        public Single JumpPowerDecay { get; private set; } = 90f;

        public Int32 JumpDirection { get; private set; } = 0;

        /// <summary>True when the user initiates a jump, while true jump force will be applied</summary>
        public Boolean Jumping { get; internal set; }

        public Boolean Falling { get; internal set; }

        private Boolean _AwaitingLandJump = true;

        /// <summary>Maximum amount of JumpCharges which are allowed to be accumulated</summary>
        public Int32 MaxJumpCharges { get; private set; } = 1;
        /// <summary>The current amount of JumpCharges that are left</summary>
        public Int32 JumpCharges { get; set; }
        /// <summary>Whether this character has any JumpCharges left</summary>
        public Boolean CanJump => JumpCharges > 0;

        public void AddJumpCharge(Int32 count)
        {
            JumpCharges += count;
            if (JumpCharges < 0) JumpCharges = 0;
            if (JumpCharges > MaxJumpCharges) JumpCharges = MaxJumpCharges;
        }

        public void ResetJumpCharges()
        {
            JumpCharges = 1;
        }

        #endregion

        public Single MovementSpeed { get; private set; } = 37f;

        public Single MaxMovementSpeed { get; private set; } = 300f;

        public Single Friction { get; private set; } = 0f;

        public Single BaseFrictionValue { get; private set; } = 50f;

        /// <summary>The current Health of this Character</summary>
        public Int32 Health => _Health.Current;

        /// <summary>The rect size of this Character</summary>
        public Vector2 Size { get; set; }

        /// <summary>Position to be used along with Radius to determine any distance checks</summary>
        public Vector2 CenterPosition => Position + new Vector2(0, -Size.Y / 2);

        /// <summary>Radius used for detection for any DetectService</summary>
        public Single Radius => Size.X;

        /// <summary>The current Velocity that the Character is moving in</summary>
        public Vector2 Velocity => new Vector2(VelocityX, VelocityY);

        private Single VelocityX { get; set; }
        private Single VelocityY { get; set; }

        public Vector2 Acceleration => new Vector2(_AccelerationX, _AccelerationY);

        private Single _AccelerationX;
        private Single _AccelerationY;

        /// <summary>The direction the Character is moving in, -1 left, 0 not moving, 1 right </summary>
        public Int32 MovementDirection { get; private set; }

        private Int32 _FaceDirection = 1;
        /// <summary>The direction the character is facing -1 left, 1 right</summary>
        public Int32 FacingDirection
        {
            get => _FaceDirection;
            internal set
            {
                _FaceDirection = value >= 0 ? 1 : -1;
                Visual.FaceDirection(_FaceDirection);
            }
        }

        /// <summary>Flag used for whether it can be used by any DetectServices</summary>
        public Boolean Active { get; set; } = false;

        public Vector2 Position
        {
            get => _Position;
            set
            {
                _Position = value;
                Visual.Position = value;
                DebugSprite.Position = value;
                _DebugPositionDot.Position = value;
            }
        }

        internal Character(String name, Vector2 size) : base(name)
        {
            Size = size;
            DebugSprite = new Sprite(CanvasService.GameCanvas, ZService.Instance["Character"], Texture.GetPixel())
            {
                Size = size,
                Offset = new Vector2(0f, 0),
                Visible = false
            };

            _DebugPositionDot = new Sprite(CanvasService.GameCanvas, ZService.Instance["Character"] + 1, Texture.GetPixel())
            {
                Size = new Vector2(5, 5),
                Offset = new Vector2(2.5f, 2.5f),
                Colour = new Vector4(0, 0, 0, 1),
                Visible = false
            };

            _PetPoly = new Sprite(CanvasService.GameCanvas, 1000, Texture.GetPixel())
            {
                Size = new Vector2(20, 20),
                Visible = false
            };

            Visual = new CharacterVisual();

            _Controller = new CharacterController(this);

            _Health = new HealthComponent(3);

            AttackService.Instance.Register((IAttacker)this);
            AttackService.Instance.Register((IAttackable)this);
            InteractService.Instance.Register(this);

            SnapShotService.Instance.Register(this, TimeSpan.FromSeconds(0.25));

            EventManager.Instance.StartListen(SceneService.OnActiveSceneChanged, SceneChanged);

            JumpCharges = MaxJumpCharges;
        }

        private void SceneChanged(Object[] obj)
        {
            _TileMap = SceneService.Instance.ActiveScene.TileMap;

            _GridCoords = _TileMap?.GetGridCoords(Position) ?? new[] { 0, 0 };

            if (SceneService.Instance.ActiveScene.Name.StartsWith("Level")) return;
            _Health.ResetToMax();
        }

        #region TileMap
        private void DetectContact()
        {
            TouchingLeft = TouchingRight = TouchingUp = Grounded = false;
            LeftTile = RightTile = UpTile = GroundTile = null;

            Vector2[] playerPositions =
            {
                Position + new Vector2(-1, 1),//Left, Up
                Position + new Vector2( 0, 1),//Up
                Position + new Vector2( 1, 1),//Right, Up
                Position + new Vector2(-1, 0),//Left
                Position + new Vector2( 1, 0),//Right
                Position + new Vector2(-1,-1),//Left, Down
                Position + new Vector2( 0,-1),//Down
                Position + new Vector2( 1,-1),//Right, Down
            };

            Int32 gridX = _GridCoords[0];
            Int32 gridY = _GridCoords[1];

            var disable = false;
            var pureGrounded = false;

            foreach (var pos in playerPositions)
            {
                var collidedTiles = _TileMap.CollisionDetect(pos, Size);

                foreach (var colTile in collidedTiles)
                {
                    if (colTile.GridY < gridY)
                    {
                        TouchingUp = true;
                        UpTile = colTile;
                    }
                    else if (colTile.GridY > gridY)
                    {
                        Grounded = true;
                        GroundTile = colTile;
                        if (colTile.GridX == gridX)
                        {
                            pureGrounded = true;
                        }
                    }

                    if (colTile.GridX < gridX && colTile.GridY == gridY)
                    {
                        TouchingLeft = true;
                        LeftTile = colTile;
                        disable = true;
                    }
                    else if (colTile.GridX > gridX && colTile.GridY == gridY)
                    {
                        TouchingRight = true;
                        RightTile = colTile;
                        disable = true;
                    }
                }
            }

            if (disable)
            {
                if (!pureGrounded)
                {
                    Grounded = false;
                    GroundTile = null;
                }

                TouchingUp = false;
                UpTile = null;
            }

            if (pureGrounded)
            {
                Grounded = true;
            }
        }
        #endregion

        #region UserActions
        internal void Interact()
        {
            Interacting = true;
            var interactables = InteractService.Instance.GetDetectables(this);
            if (interactables.Any()) interactables.First()?.Interacted(this);
        }

        internal void Attack()
        {
            Attacking = true;
            var attackables = AttackService.Instance.GetDetectables(this);
            if (attackables.Any()) attackables.First()?.Attacked(this);
        }

        internal void Crouch(Action actionComplete = null)
        {
            Crouching = true;
            Jumping = false;
            MovementDirection = 0;
            Friction = 0.2f;
        }

        internal void Move(Int32 direction)
        {
            direction = direction >= 0 ? 1 : -1;
            MovementDirection = direction;
            FacingDirection = direction;
            Friction = 0f;
        }

        internal void StopMove()
        {
            MovementDirection = 0;
            JumpDirection = 0;
            Friction = 0f;
        }

        internal void Idle()
        {
            Jumping = false;
            Crouching = false;
            MovementDirection = 0;
            Friction = 0f;
        }

        public void Attacked(IAttacker attacker)
        {
            _Health.RemoveHealth(1);
            EventManager.Instance.Trigger(DamageTaken, Name, 1);
            if (_Health.Current > 0) return;
            LevelService.Instance.LevelUnsucessful();
        }

        #endregion

        #region Jumping
        internal void StartJump(Int32 jumpDirection = 0)
        {
            AddJumpCharge(-1);

            Jumping = true;
            Falling = false;
            Crouching = false;

            FacingDirection = JumpDirection = jumpDirection;

            _AwaitingLandJump = true;
        }

        internal void StartFall()
        {
            Falling = true;
            Jumping = false;
        }

        internal void LandJump()
        {
            Jumping = false;
            Falling = false;
            ResetJumpCharges();
            StopMove();
        }
        #endregion

        #region SnapShot
        public ExpandoObject GetSnapShot()
        {
            dynamic obj = new ExpandoObject();
            obj.Position = Position;
            obj.AccelerationX = _AccelerationX;
            obj.AccelerationY = _AccelerationY;
            obj.VelocityX = VelocityX;
            obj.VelocityY = VelocityY;
            obj.Size = Size;
            return obj;
        }

        public void ApplySnapShot(ExpandoObject snapShot)
        {
            dynamic obj = snapShot;
            Position = obj.Position;
            _AccelerationX = obj.AccelerationX;
            _AccelerationY = obj.AccelerationY;
            VelocityX = obj.VelocityX;
            VelocityY = obj.VelocityY;
        }
        #endregion

        public override Boolean CanUpdate()
        {
            return _TileMap != null && base.CanUpdate();
        }

        public override void Update()
        {
            base.Update();

            _Controller.Update();
            CharacterUpdate();
            Visual.Update();

            Position = Position;

            if (SnapShotService.Instance.GetDetails(this).GetSnapShotsFrom(TimeSpan.FromSeconds(1), out ExpandoObject snapShot))
            {
                dynamic dSnap = snapShot;
                _PetPoly.Position = dSnap.Position;
            }

            Output();
        }

        private void CharacterUpdate()
        {
            Single delta = (Single)GameTime.DeltaTime.TotalSeconds;

            DetectContact();

            if (_AwaitingLandJump && Grounded && !Jumping)
            {
                _AwaitingLandJump = false;
                LandJump();
            }

            _AccelerationX = GetHorizontalAcceleration();
            _AccelerationY = GetVerticalAcceleration();

            //Apply friction to the force trying to accelerate the character
            if (MovementDirection != 0 && Grounded && !Jumping && Math.Abs(_AccelerationX) > 0)
            {
                if (GroundTile != null) _AccelerationX *= GroundTile.Friction;
                //Do not use the Character's own Friction here, because it affects gameplay too much, only use Character friction when stopping.
            }

            VelocityX += _AccelerationX;
            VelocityY = _AccelerationY;

            //Apply friction to slow the Character to a stop, not instantly
            if (MovementDirection == 0 && Grounded && !Jumping && Math.Abs(VelocityX) > 0)
            {
                var direction = VelocityX > 0 ? 1 : -1;
                var p = VelocityX > 0;

                if (GroundTile != null) VelocityX += (BaseFrictionValue * GroundTile.Friction) * -direction;
                VelocityX += BaseFrictionValue * Friction * -direction;

                //If friction would cause you to change direction, you are no longer moving
                if (p && VelocityX < 0) VelocityX = 0;
            }

            //Check if you are going so slowly you might as well stop
            if (VelocityX < 0 && VelocityX > -0.001f || VelocityX > 0 && VelocityX < 0.001f) VelocityX = 0f;
            //Check you aren't exceeding maxiumum velocity allowed
            else
            {
                VelocityX = Math.Max(VelocityX, -MaxMovementSpeed);
                VelocityX = Math.Min(VelocityX, MaxMovementSpeed);
            }

            //Have the TileMap resolve your collisions in your desired Velocity
            Position = _TileMap?.AttemptMovement(Position, Size, Velocity * delta) ?? Position;

            //Recalculate TileMap details for this Character with the new position restrains from the TileMap
            _GridCoords = _TileMap?.GetGridCoords(Position) ?? new[] { 0, 0 };

            //If you are touching any sides, then cancel your velocity/acceleration
            if (TouchingUp && VelocityY > 0 ||
                Grounded && VelocityY < 0)
            {
                VelocityY = _AccelerationY = 0f;
            }
            if (TouchingLeft && VelocityX < 0 ||
                TouchingRight && VelocityX > 0)
            {
                VelocityX = _AccelerationX = 0f;
            }
        }

        private Single GetHorizontalAcceleration()
        {
            var direction = JumpDirection != 0 ? JumpDirection : MovementDirection;

            if (Jumping)
            {
                return MaxMovementSpeed * direction;
            }

            return MovementSpeed * direction;
        }

        private Single GetVerticalAcceleration()
        {
            var gravity = Constants.World.GRAVITY * Constants.World.GravityMultiplier;

            var ay = gravity;

            //If you are jumping, then store the jump power. Invert gravity to it to negate gravity force as you jump.
            //This allows you to tweak jump better as a force on its own
            if (Jumping)
            {
                CurrentJumpPower = JumpPower + (-gravity);
            }
            else
            {
                //When you aren't jumping, reduce the power of your jump by its decay value.
                //This brings you back down slowly, instead of instantly
                CurrentJumpPower -= JumpPowerDecay;
                if (CurrentJumpPower < 0) CurrentJumpPower = 0;
            }

            //Add all the forces you calculated into the Velocity of the Character
            ay += CurrentJumpPower;

            Single WallFriction()
            {
                //Friction generated by hitting the side of a tile
                var sideFriction = 1f;
                //Set friction for touching a tile to your side
                if (LeftTile != null) sideFriction = 1 - LeftTile.SideFriction;
                else if (RightTile != null) sideFriction = 1 - RightTile.SideFriction;
                return sideFriction;
            }

            if (!Jumping)
            {
                ay *= WallFriction();
            }

            return ay;
        }

        public override void Dispose()
        {
            base.Dispose();
            _Controller.Dispose();
            Visual.Dispose();
            DebugSprite.Dispose();
            _DebugPositionDot.Dispose();

            AttackService.Instance.UnRegister((IAttacker)this);
            AttackService.Instance.UnRegister((IAttackable)this);
            InteractService.Instance.UnRegister(this);

            EventManager.Instance.StopListen(SceneService.OnActiveSceneChanged, SceneChanged);
        }

        public override String ToString()
        {
            return Name;
        }

        public void Output()
        {
            if (!Constants.World.ConsoleOutput) return;

            Console.Clear();

            Console.WriteLine("Presence Information:\n");
            Console.WriteLine($"Position: {Position}");
            Console.Write($"Grid {_GridCoords[0]}, {_GridCoords[1]}");

            Console.WriteLine("");

            Console.WriteLine($"Touching Left = {TouchingLeft}");
            Console.WriteLine($"Touching Right = {TouchingRight}");
            Console.WriteLine($"Touching Up = {TouchingUp}");
            Console.WriteLine($"Grounded = {Grounded}");

            Console.WriteLine($"JumpCharges - {JumpCharges}");
            Console.WriteLine("");

            _Controller.Output();

            Console.WriteLine("");

            Visual.Output();
        }

    }
}
