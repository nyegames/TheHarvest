﻿using System;
using AmosShared.Base;
using AmosShared.Interfaces;
using OpenTK;

namespace Game.Desktop.Camera
{
    /// <summary> The camera that looks at an object </summary>
    public class FollowCamera : AmosShared.Graphics.Camera, IUpdatable
    {
        /// <summary>The maximum distance</summary>
        private Single _MaxDistance;
        private Single _MinDistance;

        private Single _Speed = 1;

        private OpenTK.Vector2 _LookAtOffset;

        /// <summary> Whether or not the camera has been disposed </summary>
        public Boolean IsDisposed { get; set; }

        protected IPositionable _Target;

        public OpenTK.Vector2 Size => _Dimensions;

        public FollowCamera(Vector2 position, Vector2 dimensions) : base(position, dimensions)
        {
            UpdateManager.Instance.AddUpdatable(this);
        }

        public void SetFollowDistance(Single minDistance, Single maxDistance)
        {
            _MinDistance = minDistance;
            _MaxDistance = maxDistance;
        }

        public void SetSpeed(Single speed)
        {
            _Speed = speed;
        }

        public void SetLookAtOffset(OpenTK.Vector2 lookatOffset)
        {
            _LookAtOffset = lookatOffset;
        }

        public void SetTarget(IPositionable target) => _Target = target;

        /// <summary> Updates the camera's position </summary>
        /// <param name=""></param>
        public void Update()
        {
            if (_Target == null) return;

            var vector = _Target.Position - (Position + _LookAtOffset);
            var distance = vector.Length;

            var speed = _Speed * (distance * 0.05f);

            if (distance > _MinDistance || distance >= _MaxDistance)
            {
                Position += (vector.Normalized() * speed) * (Single)GameTime.DeltaTime.TotalSeconds;
            }
        }

        /// <summary> Whether or not the camera can be updated </summary>
        /// <returns></returns>
        public Boolean CanUpdate() => !IsDisposed;

        /// <summary> Disposes of the camera </summary>
        public void Dispose()
        {
            if (IsDisposed) return;
            UpdateManager.Instance.RemoveUpdatable(this);
            IsDisposed = true;
        }
    }
}
