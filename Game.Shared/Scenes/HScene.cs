﻿using System;
using System.Collections.Generic;
using System.Linq;
using AmosShared.Graphics.Drawables;
using AmosShared.Interfaces;
using Game.Desktop.HObjects;
using Game.Desktop.HObjects.TileMap;
using Game.Desktop.Interfaces;
using Game.Desktop.Interfaces.Core;

namespace Game.Desktop.Scenes
{
    public sealed class HScene : HObject
    {
        private HTileMap _TileMap;
        public HTileMap TileMap => _TileMap;

        private readonly List<IDisposable> _Disposables = new List<IDisposable>();
        private readonly List<IVisible> _Visibles = new List<IVisible>();
        private readonly List<IUpdatable> _Updatables = new List<IUpdatable>();

        private readonly Dictionary<String, Object> _Objects = new Dictionary<String, Object>();

        public List<Object> Objects => _Objects.Values.ToList();


        public override Boolean Visible
        {
            set
            {
                base.Visible = value;
                foreach (var v in _Visibles.ToArray())
                {
                    if (v is Drawable d) d.ParentVisible = value;
                    v.Visible = value;
                }
            }
        }

        public HScene(String name, Dictionary<String, Object> objects) : base(name)
        {
            if (objects != null)
            {
                foreach (var data in objects)
                {
                    if (data.Key.Equals("TileMap")) AddTileMap(data.Value as HTileMap);
                    else AddObject(data.Value, data.Key);
                }
            }
        }

        public override void Update()
        {
            base.Update();
            foreach (var u in _Updatables.Where(s => s.CanUpdate()).ToArray())
            {
                u.Update();
            }
        }

        private void AddDisposable(IDisposable disposable) => _Disposables.Add(disposable);
        private void AddVisible(IVisible visible) => _Visibles.Add(visible);
        private void AddUpdatable(IUpdatable updatable) => _Updatables.Add(updatable);

        private void AddTileMap(HTileMap tileMap)
        {
            _TileMap = tileMap;
            AddDisposable(tileMap);
        }

        public void AddObject(Object obj, String referenceName = null)
        {
            referenceName = referenceName ?? $"{obj.GetType()}_{new Guid()}";

            _Objects.Add(referenceName, obj);

            if (obj is IUpdatable u) AddUpdatable(u);
            if (obj is IVisible v) AddVisible(v);
            if (obj is IDisposable d) AddDisposable(d);

            if (obj is ICollidable col)
            {
                _TileMap?.AddCollidable(col);
            }
            if (obj is HTileMap tm) _TileMap = tm;
        }

        public Object GetObject(String name) => _Objects.ContainsKey(name) ? _Objects[name] : null;

        public Sprite GetSprite(String name) => GetObject(name) as Sprite;

        public override void Dispose()
        {
            base.Dispose();
            foreach (var d in _Disposables.ToArray()) d.Dispose();
        }
    }
}
