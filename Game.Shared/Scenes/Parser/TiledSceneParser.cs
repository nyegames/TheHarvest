﻿using System;
using System.Collections.Generic;
using System.IO;
using Game.Desktop.HObjects.TileMap;
using Game.Desktop.HObjects.TileMap.Tile;
using Game.Desktop.Parsers;
using Game.Desktop.Tiled;
using OpenTK;
using Object = System.Object;

namespace Game.Shared.Scenes.Parser
{
    class TiledSceneParser : SceneParser
    {
        internal override Dictionary<String, Object> ParseObjects(String filePath)
        {
            Dictionary<String, System.Object> objects = new Dictionary<String, System.Object>();

            TileMap tileMap = TileMap.FromJson(File.ReadAllText(filePath));
            //TODO then parse this tileMap object into an actual tile map, and add it into the HScene as a HTileMap
            //TODO find the Layer in the tileMap named "Objects" and then parse each indvidual object as a HObject, and add it into the scene

            foreach (Layer layer in tileMap.Layers)
            {
                switch (layer.Type)
                {
                    case "tilelayer":
                        {
                            objects.Add("TileMap", CreateHTileMap(layer, new Vector2(tileMap.Tilewidth, tileMap.Tileheight)));
                            break;
                        }

                    case "objectgroup":
                        {
                            if (layer.Name.StartsWith("PathingLayer"))
                            {
                                //TODO Create a parsing for creating a path finding layer in the Scene
                            }
                            else if (layer.Name.StartsWith("ObjectsLayer"))
                            {
                                foreach (var o in layer.Objects)
                                {
                                    var so = Create(o.Type, CreateObjectData(o));
                                    if (so != null) objects.Add(so.Name, so.Object);
                                }
                            }
                            break;
                        }
                }
            }

            return objects;
        }

        private Dictionary<String, Object> CreateObjectData(Desktop.Tiled.Object o)
        {
            return new Dictionary<String, Object>
            {
                {"Type", o.Type},
                {"Name", o.Name},
                {"Position", new Vector2((Single)o.X,-(Single)o.Y)},
                {"Size", new Vector2((Single)o.Width,(Single)o.Height) },
                {"Rotation", o.Rotation },
                { "Visible", o.Visible}
        };
        }


        private HTileMap CreateHTileMap(Layer layer, Vector2 tileSize)
        {
            Int64 w = (Int64)layer.Width;
            Int64 h = (Int64)layer.Height;

            var s = w * h;
            Int64[] tiles = new Int64[s];
            Int64[] data = layer.Data;

            HTile[][] hTiles = new HTile[w][];
            Int64[][] tileData = new Int64[w][];

            for (var x = 0; x < w; x++)
            {
                hTiles[x] = new HTile[h];
                tileData[x] = new Int64[h];
            }

            var tileParser = new HTileParser();

            for (int i = 0; i < s; i++)
            {
                var x = i % w;
                var y = i / h;

                tileData[y][x] = data[i];
                hTiles[y][x] = tileParser.Create(data[i] - 1, x, y, tileSize);

                hTiles[y][x].Position = new OpenTK.Vector2(tileSize.X * x, -tileSize.Y * y);
            }

            return new HTileMap(hTiles, (Int32)tileSize.X);
        }
    }
}
