﻿using System;
using System.Collections.Generic;
using Game.Desktop.HObjects;
using Game.Desktop.HObjects.Doors.Handlers;
using Game.Desktop.HObjects.EndPortals.Handlers;
using Game.Desktop.HObjects.LevelSelectors.Handlers;
using Game.Desktop.HObjects.SoulFragment.Handlers;
using Game.Desktop.HObjects.SoulHolders.Handlers;
using Game.Desktop.HObjects.SoulVessels.Handlers;
using Game.Desktop.HObjects.Spawn.Handlers;
using Game.Desktop.HObjects.TileMap.Handlers;

namespace Game.Desktop.Parsers
{
    public abstract class SceneParser
    {
        public static List<ObjectHandler> Handlers
        {
            get
            {
                var handlers = new List<ObjectHandler>()
                {
                    new DoorHandler(),
                    new SoulFragmentHandler(),
                    new LevelSelectorHandler(),
                    new CharacterSpawnerHandler(),
                    new SoulVesselHandler(),
                    new EndPortalHandler(),
                    new TileMapHandler(),
                    new SoulBoxHandler()
                };
                return handlers;
            }
        }

        protected List<ObjectHandler> _Handlers;

        protected SceneParser(IEnumerable<ObjectHandler> handlers = null)
        {
            _Handlers = Handlers;
            if (handlers == null) return;
            _Handlers.AddRange(handlers);
        }

        internal abstract Dictionary<String, Object> ParseObjects(String filePath);

        protected SceneObject Create(String name, Dictionary<String, Object> data)
        {
            foreach (var h in _Handlers)
            {
                var o = h.Create(name, data);
                if (o != null) return o;
            }

            throw new Exception($"No handler was included which can handle the creation of the object type {name}");
        }
    }
}
