﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Desktop.Parsers;
using Game.Desktop.Scenes;
using Game.Shared.Scenes.Parser;
using Object = System.Object;

namespace Game.Shared.Scenes
{
    public class SceneTiledLoader
    {
        public HScene Load(String tiledJsonPath, SceneParser parser = null)
        {
            if(parser == null) parser = new TiledSceneParser();
            var entry = System.Reflection.Assembly.GetEntryAssembly();
            String folder = $"{entry.Location}".TrimEnd(entry.ManifestModule.Name.ToCharArray());
            String filePath = $"{folder}\\{tiledJsonPath}";

            var split = filePath.Split('/');
            var last = split.Last().Split('.');
            String name = last.First();

            Dictionary<String, Object> objects = parser.ParseObjects(filePath);
            return new HScene(name, objects);
        }
    }
}
