﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using AmosShared.Base;
using AmosShared.Interfaces;
using Game.Desktop.Interfaces;
using Game.Desktop.Services.SnapShot;

namespace Game.Desktop.Tracking
{
    /// <summary>A target that will have its information tracked via the <see cref="SnapShotService"/></summary>
    public class TargetDetails : IUpdatable
    {
        private TimeSpan _CurrentTime;

        private readonly List<dynamic> _SnapShots = new List<dynamic>();

        /// <summary>The target being tracked</summary>
        public ISnapShottable Target { get; private set; }
        /// <summary>The time to wait before geneating a new <see cref="SnapShot"/> for the given <see cref="Target"/></summary>
        public TimeSpan Interval { get; private set; }

        public Boolean IsDisposed { get; set; }

        public TargetDetails(ISnapShottable target, TimeSpan interval)
        {
            Target = target;
            Interval = interval;
        }

        internal void Clear()
        {
            _SnapShots.Clear();
        }

        /// <summary>If possible, will return all snapshot positions up until the given time back</summary>
        /// <param name="timeBack">The total time back you want to retrieve the snaps to</param>
        /// <param name="snapShots">All snapshots between now and back by the time given</param>
        /// <returns></returns>
        internal Boolean GetAllSnapShotsFrom(TimeSpan timeBack, out dynamic[] snapShots)
        {
            snapShots = new dynamic[0];

            if (!_SnapShots.Any() || SnapShotService.TotalDuration - _SnapShots[0].TimeStamp < timeBack)
            {
                return false;
            }

            //Loop through all the snap shots, if the time difference is more than the duration.
            //Then all the snapshots, including the one you found, need to be returned
            for (Int32 i = _SnapShots.Count - 1; i >= 0; i--)
            {
                var age = SnapShotService.TotalDuration - _SnapShots[i].TimeStamp;
                if (age < timeBack) continue;

                List<dynamic> listSnaps = new List<dynamic>();
                //Return all the previous snapshots in an array back to the caller
                for (Int32 j = i; j < _SnapShots.Count(); j++) listSnaps.Add(_SnapShots[j]);

                snapShots = listSnaps.ToArray();
                return true;
            }

            return false;
        }

        /// <summary>Retreives a <see cref="SnapShot"/> which is was created aprox. the amount of time back as the Time requested.
        /// Return true if successfull, otherwise false</summary>
        /// <param name="timeBack">Total time back you want to retrieve the snapshot from</param>
        /// <param name="snapShot">The snapshot that will be returned if successful</param>
        /// <returns></returns>
        public Boolean GetSnapShotsFrom(TimeSpan timeBack, out ExpandoObject snapShot)
        {
            snapShot = null;

            if (!_SnapShots.Any() || SnapShotService.TotalDuration - _SnapShots[0].TimeStamp < timeBack)
            {
                return false;
            }

            //Loop through all the snap shots, if the time difference is more than the duration.
            //Then all the snapshots, including the one you found, need to be returned
            for (Int32 i = _SnapShots.Count - 1; i >= 0; i--)
            {
                var age = SnapShotService.TotalDuration - _SnapShots[i].TimeStamp;
                if (age < timeBack) continue;
                snapShot = _SnapShots[i];
                return true;
            }

            return false;
        }

        /// <summary>Will retrieve the SnapShots from the given index, up until the most recent</summary>
        /// <param name="pathIndex1"></param>
        /// <param name="pathIndex2"></param>
        /// <returns></returns>
        public List<dynamic> GetSnapShotsFrom(Int32 prevSize, out Int32 pathSize)
        {
            pathSize = _SnapShots.Count - 1;
            if (prevSize < 0) prevSize = 0;
            if (prevSize > _SnapShots.Count) prevSize = _SnapShots.Count;

            List<dynamic> snaps = new List<dynamic>();

            for (Int32 i = prevSize; i <= pathSize; i++)
            {
                snaps.Add(_SnapShots[i]);
            }

            return snaps;
        }

        public void Update()
        {
            _CurrentTime += GameTime.DeltaTime;
            if (_CurrentTime < Interval) return;
            _CurrentTime = TimeSpan.Zero;

            dynamic snap = Target.GetSnapShot();
            snap.Name = "Lartghvbh";
            snap.TimeStamp = SnapShotService.TotalDuration;
            _SnapShots.Add(snap);
        }

        public Boolean CanUpdate() => !IsDisposed;

        public void Dispose()
        {
            IsDisposed = true;
        }

        public override String ToString()
        {
            return $"{Target.ToString()}-Details";
        }
    }
}
