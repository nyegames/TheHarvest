﻿using System;
using AmosShared.Interfaces;
using OpenTK;

namespace Game.Desktop.Interfaces
{
    public interface ICollidable : IPositionable, ISizable
    {
        Boolean Collide(Vector2 bPos, Vector2 bSize);
    }

    public static class CollideUtils
    {
        public static Boolean AABB(Vector2 aPos, Vector2 aSize, Vector2 bPos, Vector2 bSize)
        {
            return aPos.X < bPos.X + bSize.X && aPos.X + aSize.X > bPos.X && aPos.Y < bPos.Y + bSize.Y && aPos.Y + aSize.Y > bPos.Y;
        }

        public static Boolean AABB(ICollidable a, ICollidable b)
        {
            return AABB(a.Position, a.Size, b.Position, b.Size);
        }

        public static Boolean AABB(ICollidable a, Vector2 bPos, Vector2 bSize)
        {
            return AABB(a.Position, a.Size, bPos, bSize);
        }
    }
}
