﻿namespace Game.Desktop.Interfaces.Core
{
    public interface ICenterPositionable
    {
        OpenTK.Vector2 CenterPosition { get; }
    }
}
