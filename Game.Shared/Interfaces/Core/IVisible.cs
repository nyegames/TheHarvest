﻿using System;

namespace Game.Desktop.Interfaces.Core
{
    public interface IVisible
    {
        Boolean Visible { get; set; }
    }
}
