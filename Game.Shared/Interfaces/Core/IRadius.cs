﻿using System;

namespace Game.Desktop.Interfaces.Core
{
    public interface IRadius
    {
        Single Radius { get; }
    }
}
