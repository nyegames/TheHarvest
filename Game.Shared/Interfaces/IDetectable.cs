﻿using System;
using Game.Desktop.Interfaces.Core;

namespace Game.Desktop.Interfaces
{
    /// <summary>A Detectable is the base class for any object which will be required to be detected somewhere in the game scene</summary>
    public interface IDetectable : ICenterPositionable, IRadius
    {
        Boolean Active { get; set; }
    }
}
