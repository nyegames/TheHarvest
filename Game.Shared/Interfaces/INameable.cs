﻿using System;

namespace Game.Desktop.Interfaces
{
    public interface INameable
    {
        String Name { get; }
    }
}
