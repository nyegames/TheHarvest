﻿using System;

namespace Game.Desktop.Interfaces
{
    public interface ICollectible : IDetectable, System.IDisposable
    {
        String Name { get; }

        void Collect();

    }
}
