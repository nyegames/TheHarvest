﻿namespace Game.Desktop.Interfaces
{
    public interface IAttackable : IDetectable
    {
        void Attacked(IAttacker attacker);
    }
}
