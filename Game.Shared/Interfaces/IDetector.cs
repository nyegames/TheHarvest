﻿using Game.Desktop.Interfaces.Core;

namespace Game.Desktop.Interfaces
{
    /// <summary>A IDetector will be an object which will search for <see cref="IDetectable"/>'s</summary>
    public interface IDetector : ICenterPositionable
    {

    }
}
