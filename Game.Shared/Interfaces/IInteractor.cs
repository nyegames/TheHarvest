﻿namespace Game.Desktop.Interfaces
{
    /// <summary>Object which can initiate an interaction with <see cref="InteractService"/></summary>
    public interface IInteractor : IDetector
    {

    }
}
