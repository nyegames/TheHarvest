﻿using System;

namespace Game.Desktop.Interfaces
{
    /// <summary>An object which can be interacted with via the <see cref="InteractService"/></summary>
    public interface IInteractable : IDetectable
    {
        /// <summary>What would happen if you were interacted with, summed up in human readable text</summary>
        /// <returns></returns>
        String InteractInstruction();

        /// <summary>You have been interacted with by the given <see cref="IInteractor"/></summary>
        /// <param name="interactor"></param>
        void Interacted(IInteractor interactor);
    }
}
