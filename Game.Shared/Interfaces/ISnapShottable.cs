﻿using System.Dynamic;

namespace Game.Desktop.Interfaces
{
    public interface ISnapShottable
    {
        ExpandoObject GetSnapShot();

        void ApplySnapShot(ExpandoObject snapShot);
    }
}
