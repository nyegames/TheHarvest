﻿using System;
using AmosShared.Events;
using AmosShared.State;
using Engine.Shared.Graphics.Textures;
using Game.Desktop.Services;
using Game.Desktop.Services.Attackables;
using Game.Desktop.Services.Camera;
using Game.Desktop.Services.Canvas;
using Game.Desktop.Services.Character;
using Game.Desktop.Services.Collectibles;
using Game.Desktop.Services.Interactions;
using Game.Desktop.Services.Level;
using Game.Desktop.Services.Scenes;
using Game.Desktop.Services.UI;
using Game.Desktop.SoulReavers;
using Game.Shared.Input;

namespace Game.Desktop.States
{
    class LoadingState : State
    {
        protected override void OnEnter()
        {
            LoadGraphics();
            Initialise();
            
            SceneService.Instance.LoadLobby();

            LoadCharacter();

            ChangeState(new GameState());
        }

        private void LoadGraphics()
        {
            SpritesheetLoader.LoadSheet("Content/Packed/Graphics/ui/", "ui.png", "ui.json");
            SpritesheetLoader.LoadSheet("Content/Packed/Fonts/KenPixel/", "KenPixel.png", "KenPixel.json");

            SpritesheetLoader.LoadSheet("Content/Packed/Graphics/bluebody/", "bluebody.png", "bluebody.json");
            SpritesheetLoader.LoadSheet("Content/Packed/Graphics/items/", "items.png", "items.json");
            SpritesheetLoader.LoadSheet("Content/Packed/Graphics/tiles/", "tiles.png", "tiles.json");
            SpritesheetLoader.LoadSheet("Content/Packed/Graphics/extras/", "extras.png", "extras.json");
            SpritesheetLoader.LoadSheet("Content/Packed/Graphics/enemies/", "enemies.png", "enemies.json");

            ZService.Instance.Load("Content/ZOrders.txt");
        }

        private void Initialise()
        {
            AttackService.Instance.Initialise();
            CameraService.Instance.Initialise();
            CanvasService.Instance.Initialise();
            CharacterService.Instance.Initialise();
            CollectService.Instance.Initialise();
            KeyboardService.Instance.Initialise();
            InteractService.Instance.Initialise();
            UiService.Instance.Initialise();
            SceneService.Instance.Initialise();
            LevelService.Instance.Initialise();
            SoulReaverController.Instance.Initialise();
        }

        private void LoadCharacter()
        {
            CharacterService.Instance.Load("BlueBody");
        }

        protected override void OnExit()
        {

        }

        public override void Update()
        {

        }

        public override void Dispose()
        {

        }
    }
}
