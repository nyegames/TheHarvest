﻿using System;
using AmosShared.State;
using Game.Desktop.Services.Camera;
using Game.Desktop.Services.Character;
using Game.Desktop.Services.Scenes;
using Game.Desktop.Services.SnapShot;
using Game.Desktop.SoulReavers;

namespace Game.Desktop.States
{
    public class GameState : State
    {
        protected override void OnEnter()
        {

        }

        protected override void OnExit()
        {

        }

        public override void Update()
        {
            if (CharacterService.Instance.ActiveCharacter != null)
            {
                CharacterService.Instance.ActiveCharacter?.Update();
                SoulReaverController.Instance.Update();
                CameraService.GameCamera.Update();
                SnapShotService.Instance.Update();
            }

            if (SceneService.Instance.ActiveScene != null)
            {
                SceneService.Instance.ActiveScene?.Update();
            }
        }

        public override void Dispose()
        {

        }
    }
}
