﻿using System;

namespace Game.Desktop.Health
{
    public class HealthComponent
    {
        public Int32 Max { get; private set; }

        public Int32 Current { get; private set; }

        /// <summary>Create the Health class and give it an initial maximum amount of health it can store</summary>
        /// <param name="maxHealth"></param>
        public HealthComponent(Int32 maxHealth)
        {
            Max = maxHealth;
            Current = Max;
        }

        public void ResetToMax()
        {
            Current = Max;
        }

        /// <summary>Give the Health class an amount of health. Can only give up to the <see cref="Max"/></summary>
        /// <param name="value"></param>
        public void GiveHealth(Int32 value)
        {
            if (Current == Max) return;
            if (Current + value > Max) Current = Max;
            else Current += value;
        }

        public void RemoveHealth(Int32 value)
        {
            if (Current <= 0) return;
            Current -= value;
            if (Current <= 0)
            {
                Current = 0;
                Die();
            }
        }

        public void IncreaseMaxHealth(Int32 value)
        {
            Max += value;
        }

        public void DecreaseMaxHealth(Int32 value)
        {
            //Cannot decreased max health to 0
            if (Max - value <= 0) return;

            Max -= value;
        }

        private void Die()
        {
            Current = 0;
        }
    }
}
