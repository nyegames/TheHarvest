﻿using System;

namespace Game.Desktop.Glide
{
    public abstract class Lerper
    {
        [Flags]
        public enum Behavior
        {
            None = 0,
            Reflect = 1,
            Rotation = 2,
            RotationRadians = 4,
            RotationDegrees = 8,
            Round = 16
        }

        protected const Single DEG = 180f / (Single)Math.PI;
        protected const Single RAD = (Single)Math.PI / 180f;

        public abstract void Initialize(Object fromValue, Object toValue, Behavior behavior);
        public abstract Object Interpolate(Single t, Object currentValue, Behavior behavior);

    }
}
