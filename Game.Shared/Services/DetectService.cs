﻿using System;
using System.Collections.Generic;
using System.Linq;
using AmosShared.Service;
using Game.Desktop.Interfaces;

namespace Game.Desktop.Services
{
    /// <summary>A <see cref="DetectManagerManager{T,TOne,TTwo}"/> is a Service class which requires 2 types of interfaces.
    /// One that will function as an object to be detected, and the other will function as the object to do the discovering.</summary>
    /// <typeparam name="TOne"></typeparam>
    /// <typeparam name="TTwo"></typeparam>
    /// <typeparam name="T"></typeparam>
    public abstract class DetectService<T, TOne, TTwo> : AService<T>
        where TOne : IDetectable
        where TTwo : IDetector
    {
        protected static List<TOne> Detectable = new List<TOne>();
        protected static List<TTwo> Detector = new List<TTwo>();

        public virtual void Register(TOne detectable)
        {
            Detectable.Add(detectable);
        }

        public virtual void Register(TTwo detector)
        {
            Detector.Add(detector);
        }

        public virtual void UnRegister(TOne detectable)
        {
            Detectable.Remove(detectable);
        }

        public virtual void UnRegister(TTwo detector)
        {
            Detector.Remove(detector);
        }

        /// <summary>Will retrieve a list of the defined Detectables which can be Detected by the current Detector</summary>
        /// <param name="detector"></param>
        /// <returns></returns>
        public List<TOne> GetDetectables(TTwo detector)
        {
            List<TOne> detected = new List<TOne>();

            foreach (var detectables in Detectable.Where(s => s.Active).ToArray())
            {
                var vector = detector.CenterPosition - detectables.CenterPosition;
                if (Math.Abs(vector.Length) > detectables.Radius) continue;
                detected.Add(detectables);
            }

            return detected;
        }

        /// <summary>Retrieves a list of defined Detectors which can be Detected by the given Detectable</summary>
        /// <param name="detectable"></param>
        /// <returns></returns>
        public List<TTwo> GetDetectors(TOne detectable)
        {
            List<TTwo> detected = new List<TTwo>();

            foreach (var detector in Detector.ToArray())
            {
                var vector = detector.CenterPosition - detectable.CenterPosition;
                if (Math.Abs(vector.Length) > detectable.Radius) continue;
                detected.Add(detector);
            }

            return detected;
        }
    }
}
