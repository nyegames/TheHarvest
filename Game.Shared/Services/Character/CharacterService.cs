﻿using System;
using System.IO;
using System.Linq;
using AmosShared.Events;
using AmosShared.Service;
using Game.Desktop.HObjects.Spawn;
using Game.Desktop.Scenes;
using Game.Desktop.Services.Scenes;

namespace Game.Desktop.Services.Character
{
    /// <summary>Currently active <see cref="Character"/>'s in the game</summary>
    public class CharacterService : AService<CharacterService>
    {
        public const String CharacterLoaded = "CharacterService-CharacterLoaded";
        public const String CharacterUnLoaded = "CharacterService-CharacterUnLoaded";

        private String _CurrentName;
        private Characters.Character _Character;

        private const String CHARACTER_FOLDER = "Characters";
        private String _CharacterFolder;

        public Characters.Character ActiveCharacter => _Character;

        private OpenTK.Vector2 _SpawnPos = OpenTK.Vector2.Zero;

        public CharacterService()
        {
            var entry = System.Reflection.Assembly.GetEntryAssembly();
            String exeFolder = $"{entry.Location}".TrimEnd(entry.ManifestModule.Name.ToCharArray());
            _CharacterFolder = $"{exeFolder}\\{CHARACTER_FOLDER}";

            Directory.CreateDirectory(_CharacterFolder);

            EventManager.Instance.StartListen(SceneService.OnActiveSceneChanged, SceneLoaded);
        }

        private void SceneLoaded(Object[] obj)
        {
            var scene = obj[0] as HScene;
            _SpawnPos = (scene.Objects.FirstOrDefault(s => (s is SpawnPosition)) as SpawnPosition)?.Position ?? OpenTK.Vector2.Zero;
            if (ActiveCharacter == null) return;
            ActiveCharacter.Position = _SpawnPos;
        }

        public void Load(String name)
        {
            _CurrentName = name;
            _Character = new Characters.Character(name, new OpenTK.Vector2(20, 40));

            if (!DataExists(_Character))
            {
                Directory.CreateDirectory(Path.Combine(_CharacterFolder, name));
            }

            //Set the character to any current spawn position that has been saved, ActiveScene changes wil update the spawn position
            ActiveCharacter.Position = _SpawnPos;

            EventManager.Instance.Trigger(CharacterLoaded, ActiveCharacter.Name);
        }

        /// <summary>If there is a Character with the same name previously created</summary>
        /// <param name="character"></param>
        /// <returns></returns>
        private Boolean DataExists(Characters.Character character)
        {
            var dInfo = Directory.GetDirectories(_CharacterFolder, "*", SearchOption.TopDirectoryOnly);
            return dInfo.Any(s => s.EndsWith(character.Name));
        }

    }
}
