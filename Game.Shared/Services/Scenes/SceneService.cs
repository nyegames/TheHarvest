﻿using System;
using AmosShared.Events;
using AmosShared.Service;
using Game.Desktop.Scenes;
using Game.Shared.Scenes;

namespace Game.Desktop.Services.Scenes
{
    public class SceneService : AService<SceneService>
    {
        /// <summary>Event to trigger the loading of the next level</summary>
        public const String OnLoadNextLevel = "SceneService-LoadNextLevel";

        public const String OnLoadLobby = "SceneService-LoadLobbyLevel";

        public const String OnActiveSceneChanged = "SceneService-ActiveSceneChanged";

        public HScene ActiveScene { get; private set; }

        private void ChangeScene(String sceneFile)
        {
            ActiveScene?.Dispose();
            ActiveScene = new SceneTiledLoader().Load(sceneFile);

            EventManager.Instance.Trigger(OnActiveSceneChanged, ActiveScene);
        }

        /// <summary>Will load the player back in to the Lobby</summary>
        public void LoadLobby()
        {
            ChangeScene("Content/TileMaps/Lobby.json");
            EventManager.Instance.Trigger(OnLoadLobby);
        }

        /// <summary>Will load the player into the next Level ready to be played</summary>
        public void LoadNextLevel()
        {
            ChangeScene("Content/TileMaps/Level1.json");
            EventManager.Instance.Trigger(OnLoadNextLevel);
        }
    }
}
