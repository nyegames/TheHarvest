﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using AmosShared.Graphics.Drawables;
using AmosShared.Service;

namespace Game.Desktop.Services
{
    public class ZService : AService<ZService>
    {
        /// <summary>All the possible Z positions for all <see cref="Drawable"/> used in the entire game</summary>
        private static Dictionary<String, Int32> _ZOrders;

        /// <summary>Using a comma seperated list, supply all the names in order of ascending Z value.
        /// The <paramref name="zGap"/> will supply the incrementation between each Z value</summary>
        /// <param name="fileName"></param>
        /// <param name="zGap"></param>
        public void Load(String fileName, Int32 zGap = 10)
        {
            _ZOrders = new Dictionary<String, Int32>();

            var entry = System.Reflection.Assembly.GetEntryAssembly();
            String folder = $"{entry.Location}".TrimEnd(entry.ManifestModule.Name.ToCharArray());
            String filePath = $"{folder}\\{fileName}";

            String all = File.ReadAllText(filePath);
            var splitData = all.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            for (Int32 i = 0; i < splitData.Length; i++)
            {
                String s = splitData[i];
                s = Regex.Replace(s, @"[^\w\.\,@-]", "");
                _ZOrders.Add(s, i * zGap);
            }
        }

        /// <summary>Retrieve the Z order with the given name</summary>
        /// <param name="zName"></param>
        /// <returns></returns>
        public Int32 Get(String zName) => Instance[zName];

        /// <summary>Retrieve the Z order with the given name</summary>
        /// <param name="zName"></param>
        /// <returns></returns>
        public Int32 this[String zName]
        {
            get
            {
                if (!_ZOrders.ContainsKey(zName)) throw new MissingMemberException($"{zName} is not contained in the ZOrder file");
                return _ZOrders[zName];
            }
        }
    }
}
