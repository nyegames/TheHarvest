﻿using System;
using System.Collections.Generic;
using System.Linq;
using AmosShared.Base;
using AmosShared.Interfaces;

namespace Game.Desktop.Services
{
    public class Timer : IUpdatable
    {
        /// <summary>Current active and created <see cref="Timer"/>'s</summary>
        private static readonly Dictionary<String, List<Timer>> _ActiveActions = new Dictionary<String, List<Timer>>();

        private readonly Boolean _SelfUpdate;

        /// <summary>The current GroupName that is being used for this TimedAction</summary>
        private readonly String _GroupName;

        /// <summary>How long it takes before invoking the callback associated</summary>
        protected TimeSpan _Duration;
        /// <summary>The Action to be Invoked upon the timer running out</summary>
        protected Action _Callback;

        /// <summary>True after this object has been destroyed, after the duration has been waited</summary>
        public Boolean IsDisposed { get; set; }

        /// <summary>After the set amount of time passes, invoke the action inserted</summary>
        /// <param name="duration"></param>
        /// <param name="callback"></param>
        public Timer(TimeSpan duration, Action callback, String groupName = "", Boolean selfUpdate = true)
        {
            _Duration = duration;
            _Callback = callback;
            _SelfUpdate = selfUpdate;
            if (_SelfUpdate) UpdateManager.Instance.AddUpdatable(this);

            _GroupName = groupName;
            if (!_ActiveActions.ContainsKey(_GroupName)) _ActiveActions.Add(_GroupName, new List<Timer>());
            _ActiveActions[_GroupName].Add(this);
        }

        public void Update()
        {
            if (IsDisposed) return;
            _Duration -= GameTime.DeltaTime;
            if (_Duration > TimeSpan.Zero) return;
            _Callback?.Invoke();
            Dispose();
        }

        /// <summary>Stop counting down the callback, and decide whether or not to trigger the finish action</summary>
        /// <param name="triggerCallback"></param>
        public void Cancel(Boolean triggerCallback = true)
        {
            if (!triggerCallback) _Callback = null;
            _Duration = TimeSpan.Zero;
        }

        public void Dispose()
        {
            if (_SelfUpdate) UpdateManager.Instance.RemoveUpdatable(this);
            IsDisposed = true;
            _ActiveActions[_GroupName].Remove(this);
            if (!_ActiveActions[_GroupName].Any()) _ActiveActions.Remove(_GroupName);
        }

        /// <summary>Retrieve all <see cref="Timer"/>'s which have been registered to the given group</summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public static List<Timer> GetTimedActionGroup(String groupName)
        {
            List<Timer> actions = new List<Timer>();
            if (_ActiveActions.ContainsKey(groupName)) actions = _ActiveActions[groupName].ToList();
            return actions;
        }

        /// <summary>Call <see cref="Cancel(bool)"/> on all of the <see cref="Timer"/>'s part of the given
        /// group</summary>
        /// <param name="groupName"></param>
        /// <param name="triggerCallback"></param>
        public static void CancelGroup(String groupName, Boolean triggerCallback = true)
        {
            foreach (var timeAction in GetTimedActionGroup(groupName).ToList())
            {
                timeAction.Cancel(triggerCallback);
            }
        }

        public Boolean CanUpdate() => !IsDisposed;
    }
}
