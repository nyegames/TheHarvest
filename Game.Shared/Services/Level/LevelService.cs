﻿using System;
using System.Collections.Generic;
using System.Linq;
using AmosShared.Events;
using AmosShared.Service;
using Game.Desktop.HObjects.EndPortals;
using Game.Desktop.HObjects.SoulVessels;
using Game.Desktop.Scenes;
using Game.Desktop.Services.Scenes;

namespace Game.Desktop.Services.Level
{
    /// <summary>Container all of the information about the current level the player is playing</summary>
    public class LevelService : AService<LevelService>
    {
        public const String OnLevelLoaded = "LevelService-LevelLoaded";

        public const String LevelProgressUpdated = "LevelService-LevelProgress";
        public const String LevelConditionsMet = "LevelService-LevelConditionsMet";

        /// <summary>The current Vessels in the scene you just loaded in</summary>
        private Dictionary<String, SoulVessel> _Vessels;
        /// <summary>The required amount of vessels to collect to complete the level</summary>
        private Int32 RequiredVessels => _Vessels?.Count ?? 0;
        /// <summary>The vessel names which have been collected by the character</summary>
        private List<String> _VesselsCollected;
        /// <summary>The current amount of vessels the player has collected in this level</summary>
        private Int32 TotalVesselsCollected => _VesselsCollected.Count;

        /// <summary>Has the player collected all the vessels required to complete this level?</summary>
        public Boolean IsAllVesselsCollected => Instance.TotalVesselsCollected >= Instance.RequiredVessels;

        public override void Initialise()
        {
            base.Initialise();

            EventManager.Instance.StartListen(EndPortal.OnPortalEntered, EnterEndPortal);
            EventManager.Instance.StartListen(SceneService.OnActiveSceneChanged, SceneLoaded);
        }

        private void SceneLoaded(Object[] data)
        {
            HScene scene = (HScene)data[0];
            _Vessels = new Dictionary<String, SoulVessel>();
            _VesselsCollected = new List<String>();

            var vessels = scene.Objects.Where(s => (s is SoulVessel)).OfType<SoulVessel>().ToArray();
            foreach (var v in vessels) _Vessels.Add(v.Name, v);

            if (scene.Name.StartsWith("Level"))
            {
                EventManager.Instance.Trigger(OnLevelLoaded, scene.Name);
            }
        }

        private void EnterEndPortal(Object[] data)
        {
            LevelUnsucessful();
        }

        public void CollectSoulVessel(String vesselName)
        {
            SoulVessel v = _Vessels[vesselName];
            var name = _Vessels.FirstOrDefault(s => s.Value.Equals(v)).Key;

            if (name == null) return;
            if (_VesselsCollected.Contains(vesselName)) return;

            _VesselsCollected.Add(name);

            EventManager.Instance.Trigger(LevelProgressUpdated, TotalVesselsCollected, RequiredVessels);
            if (IsAllVesselsCollected)
            {
                EventManager.Instance.Trigger(LevelConditionsMet, TotalVesselsCollected);
            }
        }

        public void LevelSuccessful()
        {
            SceneService.Instance.LoadLobby();
        }

        public void LevelUnsucessful()
        {
            SceneService.Instance.LoadLobby();
        }
    }
}
