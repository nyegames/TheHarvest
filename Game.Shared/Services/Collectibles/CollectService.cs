﻿using System;
using System.Collections.Generic;
using System.IO;
using AmosShared.Events;
using Game.Desktop.HObjects.SoulFragment;
using Game.Desktop.Interfaces;

namespace Game.Desktop.Services.Collectibles
{
    class CollectService : DetectService<CollectService, ICollectible, ICollector>
    {
        public const String FragmentCollected = "CollectService-FragmentCollected";

        //TODO Use BinaryReader instead of plain text writer to save the collectibles persistence

        private const String COLLECT_FOLDER = "Collectables";
        private static String _CollectiblesFilePath;

        public CollectService()
        {
            var entry = System.Reflection.Assembly.GetEntryAssembly();
            String exeFolder = $"{entry.Location}".TrimEnd(entry.ManifestModule.Name.ToCharArray());
            var folder = $"{exeFolder}\\{COLLECT_FOLDER}";

            var directory = Directory.CreateDirectory(folder);
            _CollectiblesFilePath = Path.Combine(folder, "Collectables.txt");
            if (!File.Exists(_CollectiblesFilePath)) File.Create(_CollectiblesFilePath).Close();
        }

        /// <summary>This item has been collected</summary>
        /// <param name="collectible"></param>
        public void Collected(ICollectible collectible)
        {
            SetCollected(collectible, true);
            if (!(collectible is SoulFragment)) return;
            EventManager.Instance.Trigger(FragmentCollected, collectible.Name);
        }

        /// <summary>Register a <see cref="ICollectible"/>
        /// If this collectable has already been collected previously, it will be Disposed.</summary>
        /// <param name="collectible"></param>
        public override void Register(ICollectible collectible)
        {
            //base.Register(collectible);
            if (!IsRegistered(collectible))
            {
                SetCollected(collectible, false);
            }

            Detectable.Add(collectible);

            //If you are registering an already collected Collectable, then dispose of it
            if (IsCollected(collectible)) collectible.Dispose();
        }

        public String[] GetCollectablesFound()
        {
            List<String> collectables = new List<String>();
            var lines = File.ReadAllLines(_CollectiblesFilePath);
            foreach (var l in lines)
            {
                var split = l.Trim().Split('=');
                if (!Boolean.Parse(split[1])) continue;
                collectables.Add(split[0]);
            }
            return collectables.ToArray();
        }

        private String[] GetAllRegisteredCollectables()
        {
            List<String> collectables = new List<String>();
            var lines = File.ReadAllLines(_CollectiblesFilePath);
            foreach (var l in lines)
            {
                var split = l.Trim().Split('=');
                collectables.Add(split[0]);
            }
            return collectables.ToArray();
        }

        /// <summary>Check to see if this Collectable has previously been created and stored in the data file</summary>
        /// <param name="collectible"></param>
        /// <returns></returns>
        private Boolean IsRegistered(ICollectible collectible)
        {
            var lines = File.ReadAllLines(_CollectiblesFilePath);
            foreach (var l in lines)
            {
                var split = l.Trim().Split('=');
                if (split[0].StartsWith(collectible.Name))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>Retrieve this collectables persistant collect state</summary>
        /// <param name="collectible"></param>
        /// <returns></returns>
        private Boolean IsCollected(ICollectible collectible)
        {
            var lines = File.ReadAllLines(_CollectiblesFilePath);

            for (Int32 i = 0; i < lines.Length; i++)
            {
                String l = lines[i];
                var split = l.Trim().Split('=');
                //Does this collectable have data here?
                if (split[0].StartsWith(collectible.Name))
                {
                    return Boolean.Parse(split[1]);
                }
            }

            return false;
        }

        /// <summary>Set this collectables persistant collect state</summary>
        /// <param name="collectible"></param>
        /// <param name="collected"></param>
        private void SetCollected(ICollectible collectible, Boolean collected)
        {
            var lines = File.ReadAllLines(_CollectiblesFilePath);
            Boolean found = false;

            for (Int32 i = 0; i < lines.Length; i++)
            {
                String l = lines[i];
                var split = l.Trim().Split('=');
                //Does this collectable have data here?
                if (split[0].StartsWith(collectible.Name))
                {
                    found = true;
                    lines[i] = $"{split[0]}={Boolean.Parse(found.ToString())}";
                    break;
                }
            }

            if (!found)
            {
                using (StreamWriter writer = File.AppendText(_CollectiblesFilePath))
                {
                    writer.WriteLine($"{collectible.Name}={collected.ToString()}");
                }
            }
            else
            {
                String line = "";
                foreach (var l in lines) line += $"{l}\n";
                File.WriteAllText(_CollectiblesFilePath, line);
            }
        }
    }
}
