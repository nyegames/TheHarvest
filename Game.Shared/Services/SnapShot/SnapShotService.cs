﻿using System;
using System.Collections.Generic;
using AmosShared.Base;
using AmosShared.Events;
using AmosShared.Service;
using Game.Desktop.Interfaces;
using Game.Desktop.Services.Scenes;
using Game.Desktop.Tracking;

namespace Game.Desktop.Services.SnapShot
{
    /// <summary>Register a <see cref="ISnapShottable"/> target and have its vital details stored at determined time intervals</summary>
    public class SnapShotService : AService<SnapShotService>
    {
        public static TimeSpan TotalDuration { get; private set; } = TimeSpan.Zero;

        private readonly Dictionary<ISnapShottable, TargetDetails> _TargetInformation = new Dictionary<ISnapShottable, TargetDetails>();

        public Boolean IsDisposed { get; set; }

        public SnapShotService()
        {
            EventManager.Instance.StartListen(SceneService.OnActiveSceneChanged, ActiveSceneChanged);
        }

        private void ActiveSceneChanged(Object[] obj)
        {
            foreach (var info in _TargetInformation)
            {
                info.Value.Clear();
            }
        }

        public void Register(ISnapShottable target, TimeSpan interval)
        {
            _TargetInformation.Add(target, new TargetDetails(target, interval));
        }

        public void UnRegister(ISnapShottable target)
        {
            _TargetInformation.Remove(target);
        }

        public override void Update()
        {
            base.Update();
            TotalDuration += GameTime.DeltaTime;
            foreach (var targets in _TargetInformation.Values)
            {
                targets.Update();
            }
        }

        public TargetDetails GetDetails(ISnapShottable target) => _TargetInformation[target];

        public void Dispose()
        {
            IsDisposed = true;
            EventManager.Instance.StopListen(SceneService.OnActiveSceneChanged, ActiveSceneChanged);
        }
    }
}
