﻿using System;
using System.Collections.Generic;
using AmosShared.Base;
using AmosShared.Events;
using AmosShared.Interfaces;
using AmosShared.Service;
using Game.Desktop.Camera;
using Game.Desktop.Services.Character;

namespace Game.Desktop.Services.Camera
{
    public class CameraService : AService<CameraService>
    {
        private readonly Dictionary<String, AmosShared.Graphics.Camera> _Cameras = new Dictionary<String, AmosShared.Graphics.Camera>();

        public static FollowCamera GameCamera => Instance._Cameras["Game"] as FollowCamera;
        public static AmosShared.Graphics.Camera UICamera => Instance._Cameras["UI"];

        public CameraService()
        {
            _Cameras.Add("Game", new FollowCamera(new OpenTK.Vector2(0, 0), Renderer.Instance.TargetDimensions));
            _Cameras.Add("UI", new AmosShared.Graphics.Camera(new OpenTK.Vector2(0, 0), Renderer.Instance.TargetDimensions));

            EventManager.Instance.StartListen(CharacterService.CharacterLoaded, CharacterLoaded);
        }

        private static void CharacterLoaded(Object[] data)
        {
            Follow(CharacterService.Instance.ActiveCharacter);
        }

        public static void Follow(IPositionable target)
        {
            Follow(target, new OpenTK.Vector2(10, 100), 60, new OpenTK.Vector2(0, -100));
        }

        public static void Follow(IPositionable target, OpenTK.Vector2 distance, Single speed, OpenTK.Vector2 lookOffset)
        {
            GameCamera.SetTarget(target);
            GameCamera.SetFollowDistance(distance.X, distance.Y);
            GameCamera.SetSpeed(speed);
            GameCamera.SetLookAtOffset(lookOffset);
        }
    }
}
