﻿using FarseerPhysics;

namespace Game.Desktop.Services
{
    public static class VectorExtensions
    {
        public static Microsoft.Xna.Framework.Vector2 Xna(this OpenTK.Vector2 vector2)
        {
            return new Microsoft.Xna.Framework.Vector2(vector2.X, vector2.Y);
        }

        public static OpenTK.Vector2 OpenTK(this Microsoft.Xna.Framework.Vector2 vector2)
        {
            return new OpenTK.Vector2(vector2.X, vector2.Y);
        }

        public static Microsoft.Xna.Framework.Vector2 ToDisplay(this Microsoft.Xna.Framework.Vector2 vector2)
        {
            return ConvertUnits.ToDisplayUnits(vector2);
        }

        public static Microsoft.Xna.Framework.Vector2 ToSim(this Microsoft.Xna.Framework.Vector2 vector2)
        {
            return ConvertUnits.ToSimUnits(vector2);
        }
    }
}
