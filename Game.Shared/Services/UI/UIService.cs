﻿using System;
using System.Collections.Generic;
using System.Linq;
using AmosShared.Base;
using AmosShared.Events;
using AmosShared.Service;
using Game.Desktop.Glide;
using Game.Desktop.Scenes;
using Game.Desktop.Services.Scenes;
using Game.Desktop.UI;

namespace Game.Desktop.Services.UI
{
    // ReSharper disable once InconsistentNaming
    public class UiService : AService<UiService>
    {
        // ReSharper disable once InconsistentNaming
        public static Tweener UITweener = new Tweener(true);

        private readonly List<UIComponent> _Components = new List<UIComponent>();

        public override void Initialise()
        {
            base.Initialise();
            _Components.Add(new SoulFragmentCounter());
            _Components.Add(new SoulVesselCounter());

            _Components.Add(new HealthDisplay());

            _Components.Add(new InteractPrompt());
            _Components.Add(new AttackPrompt());

            EventManager.Instance.StartListen(SceneService.OnActiveSceneChanged, SceneChanged);

            SetDefaultPositions(_Components.ToList());
        }

        /// <summary>By Default 0,0 is the center of the camera, this will make 0,0 the top left of the camera screen and
        /// bot right Dimension.X, Dimension.Y</summary>
        /// <param name="position"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public OpenTK.Vector2 UISpace(OpenTK.Vector2 position)
        {
            position = new OpenTK.Vector2(position.X, -position.Y);
            return position + new OpenTK.Vector2(-(Renderer.Instance.TargetDimensions.X / 2), Renderer.Instance.TargetDimensions.Y / 2);
        }

        public void SetDefaultPositions(List<UIComponent> components)
        {
            var d = Renderer.Instance.TargetDimensions;

            components.First(s => s is SoulFragmentCounter).Position = UISpace(new OpenTK.Vector2(0, 0));
            components.First(s => s is SoulVesselCounter).Position = UISpace(new OpenTK.Vector2(d.X / 2 - 50, d.Y - 30));
            components.First(s => s is HealthDisplay).Position = UISpace(new OpenTK.Vector2(d.X / 2 - 50, 0));
        }

        private void SceneChanged(Object[] obj)
        {
            foreach (var c in _Components) c.SceneLoaded(obj[0] as HScene);
        }

        public override void Update()
        {
            base.Update();
            foreach (var c in _Components.ToArray())
            {
                c.Update();
            }
        }
    }
}
