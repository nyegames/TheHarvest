﻿using System;
using System.Collections.Generic;
using AmosShared.Graphics;
using AmosShared.Service;
using Game.Desktop.Services.Camera;

namespace Game.Desktop.Services.Canvas
{
    public class CanvasService : AService<CanvasService>
    {
        private readonly Dictionary<String, AmosShared.Graphics.Canvas> _Canvases = new Dictionary<String, AmosShared.Graphics.Canvas>();

        public static AmosShared.Graphics.Canvas GameCanvas => Instance._Canvases["Game"];
        public static AmosShared.Graphics.Canvas UICanvas => Instance._Canvases["UI"];

        public CanvasService()
        {
            _Canvases.Add("Game", new AmosShared.Graphics.Canvas(CameraService.GameCamera, 0, new Shader("Content/Shaders/VertexShader.txt", "Content/Shaders/FragmentShader.txt")));
            _Canvases.Add("UI", new AmosShared.Graphics.Canvas(CameraService.UICamera, 1, new Shader("Content/Shaders/VertexShader.txt", "Content/Shaders/FragmentShader.txt")));
        }
    }
}
